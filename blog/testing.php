<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>xibita | FTP Access Guide</title>
	<meta name="description" content="">
	<meta name="keywords" content="">

	<!-- styles & scripts -->
	<?php //include("includes/common.php"); ?>
	<!-- Included CSS Files (Uncompressed) -->
	<!--
	<link rel="stylesheet" href="/stylesheets/foundation.css">
	-->
  
	<!-- Included CSS Files (Compressed) -->
	<link rel="stylesheet" href="/stylesheets/flexslider.css">
	<link rel="stylesheet" href="/stylesheets/xibita.css">

	<script src="/javascripts/modernizr.foundation.js"></script>
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<?php $section = "ftp"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="slider">
			<img src="/images/banner-ftp.jpg" alt="banner photo" title="Tradeshow exhibits and exhibit displays  - Outstanding Show Services" />
			</div>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>FTP access guide.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div id="maincopy" class="five columns offset-by-two small-8">
			<p class="subheading">Delivering art files and project items is easy through our FTP site. Follow the instructions below to upload files for your project.</p>
			<p>If you have any questions or problems with your file transfer, please call the Graphic Department for assistance:</p>
			<ul>
				<li>604-276-2366 (West Coast)</li>
				<li>416-494-9553 (East Coast)</li>
			</ul>
			<p>Alternatively you can call us Toll Free at 1-800-663-1737 or email us at <a href="mailto:ftpadmin@xibita.com">ftpadmin@xibita.com</a>.</p>
			<h2>Upload Instructions</h2>
			<ol>
				<li>Please place all your files into a named folder (see file naming info below).</li>
				<li>Zip the folder.</li>
				<li>Upload the zipped file.</li>
			</ol>
			<p><a href="http://67.226.141.209:8080" target="_blank">CLICK HERE TO ACCESS OUR FTP</a></p>
			<p>Please complete the form</p>
			<ul>
				<li>User ID: Upload</li>
				<li>Password: 5566</li>
			</ul>
			<h2>Additional Information</h2>
			<p><strong>File Naming</strong><br>
			Please use the Project number provided to you by your Account Manager followed by your company name e.g. &ldquo;12345_CompanyName.sit&rdquo;. If a Project number is not available at the time of file transfer, please name the file with the company that will be invoiced for the project. e.g. &ldquo;MyCompany.zip&rdquo;.</p>
			<p><strong>Upload One File Only</strong><br>
			Please compress all your working files into one compressed archive. Do not send individual files. If possible, please provide files in .zip format.</p>
			<p><strong>File Formats</strong><br>
			Xibita currently uses Adobe Creative Suite 4. To ensure your files are supplied in a compatible format, please consult <a href="docs/Xibita-Artwork-Prep-Guidelines.pdf" target="_blank">Xibita Artwork Preparation Guidelines</a>.</p>
			<p><strong>Interrupted File Transfer</strong><br>
			If your file transfer stops prior to completion, please try the upload again. The transfer may be continued successfully. Please send us an email at <a href="mailto:ftpadmin@xibita.com">ftpadmin@xibita.com</a> once your file has been successfully uploaded.</p>
			<p><a href="/docs/Xibita-FTP-Upload.pdf" target="_blank">Download these instructions</a>.</p>
			<p><a href="docs/Xibita-Artwork-Prep-Guidelines.pdf" target="_blank">Download Artwork Preparation Guidelines</a>.</p>
		</div>
		<div class="two small-8 columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').orbit({
					timer:false
				});
		});
	</script>
  
</body>
</html>
