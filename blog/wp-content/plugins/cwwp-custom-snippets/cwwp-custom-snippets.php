<?php
/*
Plugin Name: Code With WP Custom Snippets
Plugin URI: http://codewithwp.com/
Description: This plugin holds custom code snippets that interact with both themes and plugins related to this website.
Author: Thomas Griffin
Author URI: http://thomasgriffinmedia.com/
Version: 1.0.0
License: GNU General Public License v2.0 or later
License URI: http://www.opensource.org/licenses/gpl-license.php
*/

/*
	Copyright 2012	 Thomas Griffin	 (email : thomas@thomasgriffinmedia.com)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License, version 2, as
	published by the Free Software Foundation.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// ======================================================================= //
//          PLACE ALL OF YOUR CUSTOM CODE BELOW THIS COMMENT BLOCK         //   
// ======================================================================= //


// remove wordpress version
function remove_version() {
  return '';
}
add_filter('the_generator', 'remove_version');

// remove wp version param from any enqueued scripts
function at_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'at_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'at_remove_wp_ver_css_js', 9999 );

// don't display info on incorrect login attempts
function wrong_login() {
  return 'Wrong username or password.';
}
add_filter('login_errors', 'wrong_login');

// disable themes/plugins file editor
// define('DISALLOW_FILE_EDIT', true);


// ======================================================================= //
//          PLACE ALL OF YOUR CUSTOM CODE ABOVE THIS COMMENT BLOCK         //   
// ======================================================================= //

add_filter( 'http_request_args', 'cwwp_hide_plugin_updates', 5, 2 );
/**
 * Removes the plugin from repo update checks to avoid errant updates.
 *
 * @since 1.0.0
 * @link http://markjaquith.wordpress.com/2009/12/14/excluding-your-plugin-or-theme-from-update-checks/
 *
 * @param array $r The request data
 * @param string $url The URL which is being pinged for updates
 * @return array $r The amended request data
 */
function cwwp_hide_plugin_updates( $r, $url ) {
	
	/** If the URL is not from the WordPress API, return the request */
	if ( 0 !== strpos( $url, 'http://api.wordpress.org/plugins/update-check' ) )
		return $r;
			
	/** Unset our plugin if it exists */
	$plugins = unserialize( $r['body']['plugins'] );
	unset( $plugins->plugins[plugin_basename( __FILE__ )] );
	unset( $plugins->active[array_search( plugin_basename( __FILE__ ), $plugins->active )] );
	$r['body']['plugins'] = serialize( $plugins );
		
	/** Return the request */
	return $r;
	
}