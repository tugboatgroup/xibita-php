<?php
/*
Plugin Name: Xibita Author Bios
Plugin URI: http://infectious.ca/
Description: Displays the list of author bios
Author: Kevin Meronuk
Version: 1
Author URI: http://infectious.ca/
*/
 
 
class XibitaAuthorBios extends WP_Widget
{
  function XibitaAuthorBios()
  {
    $widget_ops = array('classname' => 'XibitaAuthorBios', 'description' => 'Displays the list of author bios' );
    $this->WP_Widget('XibitaAuthorBios', 'Author Bios', $widget_ops);
  }
 
	// widget form creation
	function form($instance) {

		// Check values
		if( $instance) {
     		$title = esc_attr($instance['title']);
		} else {
     		$title = '';
		}
		?>

		<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'wp_widget_plugin'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
	<?php
	}
 
	// update widget
	function update($new_instance, $old_instance) {
      	$instance = $old_instance;
      	// Fields
      	$instance['title'] = strip_tags($new_instance['title']);
     	return $instance;
	}
 
	// display widget
	function widget($args, $instance) {
   	extract( $args );
   	// these are the widget options
   	$title = apply_filters('widget_title', $instance['title']);
	
	echo $before_widget;
	?>
	<div class="author-bio-widget">
	<?php echo $before_title; ?><?php echo $title; ?><?php echo $after_title; ?>
	<div id="authorlist"><?php contributors(); ?></div>
	<?php
	echo $after_widget;
	}

}
add_action( 'widgets_init', create_function('', 'return register_widget("XibitaAuthorBios");') );?>