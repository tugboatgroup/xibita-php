<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

	<div class="six columns push-three">
		<div class="row collapse">
			<div class="seven columns single-main">
				<?php while ( have_posts() ) : the_post(); ?>
	
					<?php get_template_part( 'content', 'page' ); ?>
		
				<?php endwhile; ?>
			</div>
			<div class="three columns single-side">
				<?php get_sidebar( 'secondary' ); ?>
			</div>
		</div>
	</div>
	<div class="two columns pull-seven offset-by-one">
		<?php get_sidebar( 'main' ); ?>
	</div>

<?php get_footer(); ?>