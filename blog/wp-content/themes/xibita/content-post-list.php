<?php
/**
 * The default template for displaying content.
 */
?>

	<article <?php post_class('article-wrap'); ?>>
		<?php if(has_post_thumbnail()) {?>
			<header class="article-thumb">			
				<div class="article-cat"><?php the_category(' '); ?></div>
				<div class="article-img"><?php echo get_the_post_thumbnail($post->ID, 'xibita-thumb'); ?></div>
			</header>
			<div class="article-copy">
				<h2 class="article-title"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p class="article-author">Posted by: <?php the_author_link(); ?></p>
			</div>
		<?php }
		else {?>
			<header class="article-nothumb">
				<div class="article-cat"><?php the_category(', '); ?></div>
			</header>
			<div class="article-copy">
				<h2 class="article-title"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p class="article-author">Posted by: <?php the_author_posts_link(); ?></p>
				<div class="article-excerpt">
					<?php the_excerpt(); ?>
				</div>
			</div>
		<?php }?>
	</article>
