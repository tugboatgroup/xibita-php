<?php
/**
 * The template for displaying the footer.
 */
?>
			</div>
		</div>
		<!-- end main content wrap -->
		<div id="prefooter" class="show-hide">
			<h3 class="pftr-heading">Join our conversation</h3>
			<div class="pftr-content row">
				<div class="four columns centered">
					<div id="" class="pftr-widget">
						<div class="pftr-widget-header cf">
							<div class="pftr-widget-type"><a href="https://twitter.com/Xibita1" target="_blank">Twitter</a></div>
							<div class="pftr-widget-heading"><h5><a href="https://twitter.com/Xibita1" target="_blank">Follow us @Xibita1</a></h5></div>
						</div>
						<div class="pftr-widget-content cf">
							<div class="my_tweets_block">
								<?php
									/* your parameters */
									$jltw_args = array(
										'username'	=> 'Xibita1',
										'nb_tweets'	=> 1,
										'avatar'	=> false,
										'cache'		=> 120,
										'transition'	=> false,
										'delay'		=> 8,
										'links'		=> true
									);

									/* display widget */
									jltw($jltw_args);
								?>
							</div>
						</div>
					</div>
					<div id="" class="pftr-widget">
						<div class="pftr-widget-header cf">
							<div class="pftr-widget-type"><a href="https://www.facebook.com/pages/Xibita/171499779675659" target="_blank">Facebook</a></div>
							<div class="pftr-widget-heading"><h5><a href="https://www.facebook.com/pages/Xibita/171499779675659" target="_blank">Like Us</a></h5></div>
						</div>
						<div class="pftr-widget-content cf">
							<p>Like us for exclusive content about upcoming product lines and further insights about tradeshow displays.</p>
						</div>
					</div>
					<div id="" class="pftr-widget">
						<div class="pftr-widget-header cf">
							<div class="pftr-widget-type"><a href="http://instagram.com/xibitaca" target="_blank">Instagram</a></div>
							<div class="pftr-widget-heading"><h5><a href="http://instagram.com/xibitaca" target="_blank">Follow us @Xibitaca</a></h5></div>
						</div>
						<div class="pftr-widget-content cf">
							<div class="instagram"></div>
							<script type="text/javascript">
								var accessToken = '471646092.4093d8f.11b3fa658c32441b935d00d1ac9c8153';
								var userId = 471646092;
								jQuery(".instagram").instagram({
									userId: userId,
									accessToken: accessToken,
									show: 3
								});
							</script>
						</div>
					</div>
					<div id="" class="pftr-widget">
						<div class="pftr-widget-header cf">
							<div class="pftr-widget-type"><a href="http://pinterest.com/xibitacanada/" target="_blank">Pinterest</a></div>
							<div class="pftr-widget-heading"><h5><a href="http://pinterest.com/xibitacanada/" target="_blank">Follow us</a></h5></div>
						</div>
						<div class="pftr-widget-content cf">
							<p>Follow us on Pintrest for exhibit design inspiration.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<div class="row">
				<div class="ten columns">
					<div class="row">
						<div class="six columns">
							<h6>Contact Us</h6>
							<ul id="ftr-contacts">
								<li class="headoffice"><p><strong>Vancouver</strong><br><span>Head Office</span><br>109 - 3551 Viking Way<br>Richmond, BC<br>V6V 1W1</p><p><strong>T</strong> (604) 276-2366<br><strong>F</strong> (604) 276-0860<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
								<li><p><strong>Toronto</strong><br><br>455 Milner Ave, Unit 10<br>Scarborough, ON<br>M1B 2K4</p><p><strong>T</strong> (416) 494-9553<br><strong>F</strong> (416) 494-7462<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
								<li><p><strong>Ottawa</strong><br><br><br><br><br></p><p><strong>T</strong> (613) 723-0765<br><strong>F</strong> (613) 723-5489<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
							</ul>
						</div>
						<div class="four columns">
							<h6>Sign Up for our Newsletter</h6>

							<!-- Begin MailChimp Signup Form -->
							<div id="mc_embed_signup">
							<form action="//xibita.us16.list-manage.com/subscribe/post?u=ddefb8af40a381ace542b81b5&amp;id=b24cca4368" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							    <div id="mc_embed_signup_scroll">
								
							<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
							<div class="mc-field-group">
								<label for="mce-FNAME">First Name  <span class="asterisk">*</span>
							</label>
								<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
							</div>
							<div class="mc-field-group">
								<label for="mce-LNAME">Last Name  <span class="asterisk">*</span>
							</label>
								<input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
							</div>
							<div class="mc-field-group">
								<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
							</label>
								<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
							</div>
								<div id="mce-responses" class="clear">
									<div class="response" id="mce-error-response" style="display:none"></div>
									<div class="response" id="mce-success-response" style="display:none"></div>
								</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ddefb8af40a381ace542b81b5_b24cca4368" tabindex="-1" value=""></div>
							    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
							    </div>
							</form>
							</div>

							<!--End mc_embed_signup-->

						</div>
					</div>
					<div class="row">
						<div class="ten columns centered">
							<div class="credit"><a href="http://www.creative-engine.ca/" target="_blank">Design by: <strong>Creative Engine</strong></a></div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<script>
	  new jQueryCollapse(jQuery("#prefooter"), {
		  open: function() {
			this.slideDown(150);
		},
		close: function() {
			this.slideUp(150);
		}
	  });
	</script>
	<?php wp_footer(); ?>
	<script>
		jQuery('.eqdiv').equalize({reset: true});
	</script>

</body>
</html>