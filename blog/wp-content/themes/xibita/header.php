<?php
/**
 * The Header for our theme.
 */
?><!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>

<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
</head>

<body <?php body_class(); ?>>
	<div id="main-wrap">
		<!-- start header & nav -->
		<header id="main-header">
			<div class="row">
				<div class="eight columns centered">
					<div class="row">
						<div class="ten columns">
							<nav id="utility">
								<ul>
									<li><a href="http://www.xibita.com/about">About Us</a></li>
									<li><a href="http://www.xibita.com/contact-us">Contact Us</a></li>
									<li><a href="http://www.xibita.com/ftp">FTP</a></li>
									<li class="btn-blog"><a href="/">Blog</a></li>
								</ul>
							</nav>
						</div>
					</div>
					<div id="row-logo-nav" class="row">
						<div id="logo"><a href="http://www.xibita.com/"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/xibita-logo.png" alt="Xibita logo" title="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>"></a></div>
						<nav id="primary">
							<select id="alt-nav" onChange="window.location.replace(this.options[this.selectedIndex].value)">
								<option>Navigation</option>
								<option value="http://www.xibita.com/">Xibita Home</option>
								<option value="http://www.xibita.com/exhibits">CUSTOM EXHIBITS + ENVIRONMENTS</option>
								<option value="http://www.xibita.com/retail-environments"> - Retail and Corporate Environments</option>
								<option value="http://www.xibita.com/custom-graphics"> - Custom Graphics</option>
								<option value="http://www.xibita.com/portable-displays">THE PORTABLES DISPLAY SYSTEMS</option>
								<option value="http://www.xibita.com/banner-stands"> - Roll-Up &amp; Retractable Banners</option>
								<option value="http://www.xibita.com/fabric-systems"> - Fabric Systems</option>
								<option value="http://www.xibita.com/modular-systems"> - Modular Systems</option>
								<option value="http://www.xibita.com/outdoor-products"> - Outdoor Products &amp; Accessories</option>
								<option value="http://www.xibita.com/exhibit-design-services">SERVICES</option>
								<option>MORE...</option>
								<option value="http://www.xibita.com/about">ABOUT US</option>
								<option value="http://www.xibita.com/team"> - Leadership Team</option>
								<option value="http://www.xibita.com/contact-us">CONTACT US</option>
								<option value="http://www.xibita.com/locations"> - Locations</option>
								<option value="http://www.xibita.com/ftp">FTP</option>
								<option value="/">BLOG</option>
							</select>
							<ul class="tiny-hide">
								<li id="nav-custom"><a href="http://www.xibita.com/exhibits">Custom Exhibits + Environments</a></li>
								<li id="nav-portables"><a href="http://www.xibita.com/portable-displays">The Portables Display Systems</a></li>
								<li id="nav-services"><a href="http://www.xibita.com/exhibit-design-services">Services</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->
		<!-- start subheader -->
		<div class="row">
			<div class="eight columns centered">
				<div id="subheader" class="row">
					<div class="ten columns">
						<div class="row">
							<div class="five columns offset-by-three end">
								<div id="blog-title"><a href="/">Beyond Ordinary</a></div>
							</div>
						</div>
						<div class="row">
							<div class="four columns offset-by-five end">
								<div id="blog-tagline">Cutting-edge information about<br>the field of face-to-face marketing</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end subheader -->
		<!-- start main content wrap -->
		<div id="main-content">
				<div class="row collapse">
