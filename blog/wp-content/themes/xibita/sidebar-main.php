<?php
/**
 * The Template for displaying the main sidebar
 */
?>

<div id="secondary" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar-primary' ); ?>
		</div><!-- #secondary -->