<?php
/**
 * The default template for displaying single posts.
 */
?>

	<article <?php post_class('article-single'); ?>>
		<header>			
			<?php if(has_post_thumbnail()) {?>
				<div class="article-thumb">
					<div class="article-cat"><?php the_category(' '); ?></div>
					<div class="article-img"><?php echo get_the_post_thumbnail($post->ID, 'xibita-featured'); ?></div>
				</div>
			<?php }
			else {?>	
				<div class="article-nothumb">
					<div class="article-cat"><?php the_category(' '); ?></div>
				</div>
			<?php }?>
			<div class="article-meta cf">
				<h1 class="article-title"><?php the_title(); ?></h1>
				<div class="article-avatar"><?php echo get_avatar( get_the_author_meta('email'), '50' ); ?></div>
				<div class="article-author">Posted by: <?php the_author_posts_link(); ?> | <?php the_date(); ?></div>
			</div>
		</header>
		<div class="article-copy">
			<?php the_content(); ?>
		</div>
		<nav class="nav-single cf">
			<div class="nav-previous"><?php previous_post_link( '%link', 'Previous Post' ); ?></div>
			<div class="nav-next"><?php next_post_link( '%link', 'Next Post'); ?></div>
		</nav><!-- .nav-single -->
	</article>
