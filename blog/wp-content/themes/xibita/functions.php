<?php
/**
 * Xibita functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 */
 
/**
 * Sets up theme defaults and registers the various WordPress features that
 * the Xibita theme supports.
 *
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 */
function xibita_setup() {

	// This theme styles the visual editor with editor-style.css to give it some niceties.
	add_editor_style();

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'xibita' ) );

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 500, 9999 ); // Unlimited height, soft crop
	
	// Remove custom header support: http://codex.wordpress.org/Custom_Headers
	remove_theme_support( 'custom-header' );

	// Remove support for post formats: http://codex.wordpress.org/Post_Formats
	remove_theme_support( 'post-formats' );

	// Remove featured images support: http://codex.wordpress.org/Post_Thumbnails
	//remove_theme_support( 'post-thumbnails' );

	// Remove custom background support: http://codex.wordpress.org/Custom_Backgrounds
	remove_theme_support( 'custom-background' );

	// Remove automatic feed links support: http://codex.wordpress.org/Automatic_Feed_Links
	//remove_theme_support( 'automatic-feed-links' );

	// Remove editor styles: http://codex.wordpress.org/Editor_Style
	//remove_editor_styles();

	// Remove a menu from the theme: http://codex.wordpress.org/Navigation_Menus
	//unregister_nav_menu( 'secondary' );
}
add_action( 'after_setup_theme', 'xibita_setup' );

/* custom featured image size */
	//add_image_size( 'xibita-home', 1116, 600, true );
	add_image_size( 'xibita-featured', 717, 9999 );
	add_image_size( 'xibita-thumb', 717, 496, true );

/**
 * Remove default Dashboard widgets
 */
function remove_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
  //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
  //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );


/**
 * Register sidebar widget areas
 */
function xibita_widgets_init() {

	register_sidebar( array(
		'name' => __( 'Primary Sidebar', 'xibita' ),
		'id' => 'sidebar-primary',
		'description' => __( 'Primary sidebar widget area', 'xibita' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Secondary Sidebar', 'xibita' ),
		'id' => 'sidebar-secondary',
		'description' => __( 'Secondary sidebar widget area', 'xibita' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );

}
add_action( 'widgets_init', 'xibita_widgets_init' );


/**
 * Enqueues scripts and styles for front-end.
 */
function xibita_scripts_styles() {
	global $wp_styles;
	
	/*
	 * Register styles
	 */
	wp_register_style( 'foundation-min-css', get_template_directory_uri() . '/css/foundation.min.css', array(), '', 'all' );
	
	/*
	 * Load styles
	 */
	wp_enqueue_style('foundation-min-css');

	/*
	 * Loads our main stylesheet.
	 */
	wp_enqueue_style( 'xibita-style', get_stylesheet_uri() );

	/*
	 * Optional: Loads the Internet Explorer specific stylesheet.
	 */
	//wp_enqueue_style( 'xibita-ie', get_template_directory_uri() . '/css/ie.css', array( 'xibita-style' ), '20121010' );
	//$wp_styles->add_data( 'xibita-ie', 'conditional', 'lt IE 9' );

	/*
	 * Register other scripts
	 */
	wp_register_script( 'modernizr-foundation-js', get_template_directory_uri() . '/js/modernizr.foundation.js', array(), '1.0' );
	wp_register_script( 'foundation-min-js', get_template_directory_uri() . '/js/foundation.min.js', array(), '1.0', true );
	wp_register_script( 'app-js', get_template_directory_uri() . '/js/app.js', array(), '1.0', true );
	wp_register_script( 'jquery-collapse', get_template_directory_uri() . '/js/jquery.collapse.js', array(), '1.0' );
	wp_register_script( 'jquery-collapse_cookie_storage', get_template_directory_uri() . '/js/jquery.collapse_cookie_storage.js', array(), '1.0', true );
	wp_register_script( 'jquery-collapse_storage', get_template_directory_uri() . '/js/jquery.collapse_storage.js', array(), '1.0', true );
	wp_register_script( 'jquery-equalize', get_template_directory_uri() . '/js/equalize.min.js', array(), '1.0', true );
	wp_register_script( 'jquery-instagram', 'http://cdnjs.cloudflare.com/ajax/libs/jquery-instagram/0.2.2/jquery.instagram.min.js', array(), '1.0' );
	
	/*
	 * Load other scripts
	 */
	wp_enqueue_script( 'modernizr-foundation-js' );
	wp_enqueue_script("jquery");
	wp_enqueue_script( 'foundation-min-js' );
	wp_enqueue_script( 'app-js' );
	wp_enqueue_script( 'jquery-collapse' );
//	wp_enqueue_script( 'jquery-collapse_cookie_storage' );
//	wp_enqueue_script( 'jquery-collapse_storage' );
	wp_enqueue_script( 'jquery-equalize' );
	wp_enqueue_script( 'jquery-instagram' );

	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
}

add_action( 'wp_enqueue_scripts', 'xibita_scripts_styles' );

/**
 * Posts in Two Columns
 */
add_filter('post_class','category_two_column_classes');
 
function category_two_column_classes( $classes ) {
global $wp_query;
if( is_home() or is_archive() ) :
$classes[] = 'two-column-post';
if( $wp_query->current_post%2 == 0 ) $classes[] = 'two-column-post-left';
endif;
return $classes;
}

/**
 * Conditional tag for blog-related pages
 * http://www.wprecipes.com/wordpress-tip-conditional-tag-for-blog-related-pages
 */
function is_blog() {
	if (is_home() || is_singular('post') || is_post_type_archive('post'))
		return true;
	else return false;
}

/**
 * Modify user profile fields
 */
function remove_contact( $contactmethods ) {
  unset($contactmethods['aim']);
  unset($contactmethods['jabber']);
  unset($contactmethods['yim']);
  return $contactmethods;
}
add_filter('user_contactmethods','remove_contact',10,1);

// Add profile fields
add_action( 'show_user_profile', 'extra_profile_fields',1,1 );
add_action( 'edit_user_profile', 'extra_profile_fields',1,1 );
 
function extra_profile_fields( $user ) { ?>
    <table class="form-table">
        <tr>
            <th><label for="jobtitle">Role</label></th>           
            <td>
                <input type="text" name="jobtitle" id="jobtitle" value="<?php echo esc_attr( get_the_author_meta( 'jobtitle', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description">Example: Chief Technology Officer</span>
            </td>
        <tr>
    </table>
    <h3>Social Media Profiles</h3>
    <table class="form-table">
        <tr>
            <th><label for="twitter">Twitter URL</label></th>
            <td>
                <input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description">Example: http://twitter.com/username</span>
            </td>
        </tr>
        <tr>
            <th><label for="facebook">Facebook URL</label></th>
            <td>
                <input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description">Example: http://facebook.com/username</span>
            </td>
        </tr>
        <tr>
            <th><label for="linkedin">LinkedIn URL</label></th>
            <td>
                <input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description">Example: http://linkedin.com/in/username</span>
            </td>
        </tr>
    </table>
<?php }

add_action( 'personal_options_update', 'save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_profile_fields' );
 
function save_extra_profile_fields( $user_id ) {
 
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
 
    update_user_meta( $user_id, 'jobtitle', $_POST['jobtitle'] );
    update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
    update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
    update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );       
}


/**
 * Contributor list sidebar widget function
 */
function contributors() {
	global $wpdb;
	$authors = get_users('role=author&exclude=2');

	foreach ($authors as $author ) {
		echo "<div class=\"author-bio-wrap cf\">";
		echo "<div class=\"bio-avatar\"><a href=\"".get_bloginfo('url')."/author/";
		the_author_meta('user_nicename', $author->ID);
		echo "/\">";
		echo get_avatar($author->ID, '50');
		echo "</a></div>";
		echo "<p class=\"author-bio-name\"><a href=\"".get_bloginfo('url')."/author/";
		the_author_meta('user_nicename', $author->ID);
		echo "/\">";
		the_author_meta('display_name', $author->ID);
		echo "</a></p>";
		echo "<p class=\"author-bio-title\">";
		the_author_meta('jobtitle', $author->ID);
		echo "</p>";
		echo "</div>";
	}
}

if( !function_exists('jltw_remove_follow_us')) {
	function jltw_remove_follow_us() {
		return true; // true remove the element
	}
}
add_filter('jltw_remove_follow_us_line', 'jltw_remove_follow_us');

if( !function_exists('jltw_remove_meta')) {
	function jltw_remove_meta() {
		return true; // true remove the element
	}
}
add_filter('jltw_remove_metadata', 'jltw_remove_meta');


/**
 * Change the excerpt functionality.
 */
//function new_excerpt_more( $more ) {
//	return '<br><a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Read more...</a></p>';
//}
//add_filter( 'excerpt_more', 'new_excerpt_more' );

/*************************************CODE-Z**********************************
*  @Author: Boutros AbiChedid
*  @Date: May 3, 2012
*  @Websites: bacsoftwareconsulting.com/ ; blueoliveonline.com/
*  @Description: For the automatically generated Excerpt:
1. Option to generate a variable Length Excerpt.
2. Option to generate a fixed Length Excerpt. Complete the sentence in the Excerpt.
3. Option to add a 'Continue Reading' link to the text (Excerpt ending).
4. Option to add an unlinked Ellipsis to the text (Excerpt ending).
5. Option to preserve ALL, SOME, or NONE of the HTML formatting in the Excerpt.
6. The Code counts 'real words'. Does not count the HTML tags or their content.
7. Advantage of step 6: No opened HTML tags in the Excerpt.
8. Code Ignores Manual Excerpts and use the auto-generated one instead.
*  @Tested on: WordPress version 3.3.2
****************************************************************************/
         
function bac_variable_length_excerpt($text, $length, $finish_sentence){
     //Word length of the excerpt. This is exact or NOT depending on your '$finish_sentence' variable.
     $length = 40; /* Change the Length of the excerpt as you wish. The Length is in words. */
      
     //1 if you want to finish the sentence of the excerpt (No weird cuts).
     $finish_sentence = 1; // Put 0 if you do NOT want to finish the sentence.
       
     $tokens = array();
     $out = '';
     $word = 0;
   
    //Divide the string into tokens; HTML tags, or words, followed by any whitespace.
    $regex = '/(<[^>]+>|[^<>\s]+)\s*/u';
    preg_match_all($regex, $text, $tokens);
    foreach ($tokens[0] as $t){
        //Parse each token
        if ($word >= $length && !$finish_sentence){
            //Limit reached
            break;
        }
        if ($t[0] != '<'){
            //Token is not a tag.
            //Regular expression that checks for the end of the sentence: '.', '?' or '!'
            $regex1 = '/[\?\.\!]\s*$/uS';
            if ($word >= $length && $finish_sentence && preg_match($regex1, $t) == 1){
                //Limit reached, continue until ? . or ! occur to reach the end of the sentence.
                $out .= trim($t);
                break;
            }  
            $word++;
        }
        //Append what's left of the token.
        $out .= $t;    
    }
    //Add the excerpt ending as a link.
    $excerpt_end = '<br><a href="'. get_permalink($post->ID) . '">' . 'Read more' . '</a>';
     
    //Add the excerpt ending as a non-linked ellipsis with brackets.
    //$excerpt_end = ' [&hellip;]';
     
    //Append the excerpt ending to the token.
    $out .= $excerpt_end;
     
    return trim(force_balance_tags($out));
}
 
function bac_excerpt_filter($text){
    //Get the full content and filter it.
    $text = get_the_content('');
    $text = strip_shortcodes( $text );
    $text = apply_filters('the_content', $text);
     
    $text = str_replace(']]>', ']]&gt;', $text);
     
    /**By default the code allows all HTML tags in the excerpt**/
    //Control what HTML tags to allow: If you want to allow ALL HTML tags in the excerpt, then do NOT touch.
     
    //If you want to Allow SOME tags: THEN Uncomment the next line + Line 80.
    $allowed_tags = '<p>,<a>,<strong>'; /* Here I am allowing p, a, strong tags. Separate tags by comma. */
     
    //If you want to Disallow ALL HTML tags: THEN Uncomment the next line + Line 80,
    //$allowed_tags = ''; /* To disallow all HTML tags, keep it empty. The Excerpt will be unformated but newlines are preserved. */
    $text = strip_tags($text, $allowed_tags); /* Line 80 */
     
    //Create the excerpt.
    $text = bac_variable_length_excerpt($text, $length, $finish_sentence); 
    return $text;
}
 //Hooks the 'bac_excerpt_filter' function to a specific (get_the_excerpt) filter action.
  add_filter('get_the_excerpt','bac_excerpt_filter',5);
?>
