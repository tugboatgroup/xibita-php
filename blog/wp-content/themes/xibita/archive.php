<?php
/**
 * 
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

	<div class="six columns push-three">
		<header class="archive-header">
			<h1 class="archive-title"><?php
				if ( is_day() ) :
					printf( __( 'Daily Archives: %s', 'twentytwelve' ), '<span>' . get_the_date() . '</span>' );
				elseif ( is_month() ) :
					printf( __( 'Monthly Archives: %s', 'twentytwelve' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'twentytwelve' ) ) . '</span>' );
				elseif ( is_year() ) :
					printf( __( 'Yearly Archives: %s', 'twentytwelve' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'twentytwelve' ) ) . '</span>' );
				elseif ( is_author() ) :
					printf( __( 'Author Archives: %s', 'twentytwelve' ), '<span>' . get_the_author() . '</span>' );
				else :
					printf( __( 'Category: %s', 'twentytwelve' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				endif;
			?></h1>
		</header><!-- .archive-header -->
		<div class="eqdiv">
			<?php while ( have_posts() ) : the_post(); ?>
	
				<?php get_template_part( 'content', 'post-list' ); ?>
		
			<?php endwhile; ?>
		</div>
		<?php wp_pagenavi(); ?>
	</div>
	<div class="two columns pull-six offset-by-one end">
		<?php get_sidebar( 'main' ); ?>
	</div>

<?php get_footer(); ?>