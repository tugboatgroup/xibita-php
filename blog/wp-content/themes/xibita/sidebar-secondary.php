<?php
/**
 * The Template for displaying the secondary sidebar
 */
?>

<div id="tertiary" class="widget-area" role="complementary">
			<?php
				if(is_blog()) { ?>
    				<aside class="widget">
    					<h4 class="widget-title">Share this post</h4>
    					<div class="sharing cf">
    						<a target="_blank" rel="nofollow" title="Share on Facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>&t=<?php the_title(); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icon-facebook.png" alt="Facebook" title="Share this post on Facebook"></a>
    						<a target="_blank" rel="nofollow" title="Share on Twitter" href="https://twitter.com/share?url=<?php echo wp_get_shortlink(); ?>&lang=en&text=<?php echo urlencode(the_title()); ?>&via=Xibita1&conturl=<?php echo wp_get_shortlink(); ?>&count=vertical"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icon-twitter.png" alt="Twitter" title="Share this post on Twitter"></a>
							<a target="_blank" rel="nofollow" title="Share on Pinterest" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&description=<?php the_title(); ?>" class="pin-it-button" count-layout="horizontal"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icon-pinterest.png" alt="Pinterest" title="Share this post on Pinterest"></a>
    					</div>
    				</aside>
				<?php }
			?>
			<?php
				if(is_page( array(1708, 1731))) { ?>
    				<aside class="widget">
    					<h4 class="widget-title">Share Beyond Ordinary</h4>
    					<div class="sharing cf">
    						<a target="_blank" rel="nofollow" title="Share on Facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fblog.xibita.com%2F&t=Beyond%20Ordinary%20-%20Cutting-edge%20information%20about%20the%20field%20of%20face-to-face%20marketing"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icon-facebook.png" alt="Facebook" title="Share this post on Facebook"></a>
    						<a target="_blank" rel="nofollow" title="Share on Twitter" href="https://twitter.com/share?url=http%3A%2F%2Fblog.xibita.com%2F&lang=en&text=Beyond%20Ordinary%20-%20Cutting-edge%20information%20about%20the%20field%20of%20face-to-face%20marketing&via=Xibita1&conturl=http%3A%2F%2Fblog.xibita.com%2F&count=vertical"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icon-twitter.png" alt="Twitter" title="Share this post on Twitter"></a>
							<a target="_blank" rel="nofollow" title="Share on Pinterest" href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fblog.xibita.com%2F&description=Beyond%20Ordinary%20-%20Cutting-edge%20information%20about%20the%20field%20of%20face-to-face%20marketing" class="pin-it-button" count-layout="horizontal"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/icon-pinterest.png" alt="Pinterest" title="Share this post on Pinterest"></a>
    					</div>
    				</aside>
				<?php }
			?>
			<?php dynamic_sidebar( 'sidebar-secondary' ); ?>
		</div><!-- #secondary -->