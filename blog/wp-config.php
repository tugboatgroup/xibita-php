<?php

define( 'DISALLOW_FILE_EDIT', true );

/**
* The base configurations of the WordPress.
*
* This file has the following configurations: MySQL settings, Table Prefix,
* Secret Keys, WordPress Language, and ABSPATH. You can find more information
* by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
* wp-config.php} Codex page. You can get the MySQL settings from your web host.
*
* This file is used by the wp-config.php creation script during the
* installation. You don't have to use the web site, you can just copy this file
* to "wp-config.php" and fill in the values.
*
* @package WordPress
*/

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'xibita_blogdb' );

/** MySQL database username */
define( 'DB_USER', 'xibita_blogdb' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Lz5qpYZy' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define('AUTH_KEY',         '@~Dav.w@see25qL(tq#JNj8A|`R^#(7g8XZVvLZy@jhmt/01Vgrfo<PR. @*@&&D');
define('SECURE_AUTH_KEY',  '1.iW7T=<iCjQz~|{sGo-w6,lj<zp-!O+B^i0luTQ3L$-b,zE,%N>Zr]h(*&5C-dT');
define('LOGGED_IN_KEY',    'sV|g-Ghxx.u<^<alT!8Hu41FSQ:R**vaPNh=!B.c,FM+.@@|4ozLdIP&R[_bdEn`');
define('NONCE_KEY',        '.I64!g|}J%[IUIl;xxGN-9muljH/Nmp`Qi;8wM2`~>tQf2aRJ1#*J;c;h34j=~~ ');
define('AUTH_SALT',        'px6.r [8m$ewXmB%X]~>PL`59Y`85F226TK>fG<A|n;nE$%z>{d2@>j[^^y<lgXt');
define('SECURE_AUTH_SALT', 'KPEva7 HxlDH5w,{u89^03f#lf&<p[Y;ff*:X%Ct$O1%~M*xs<tNRdftV{}Ozd.`');
define('LOGGED_IN_SALT',   '-|05}yeMN{[k-Hg&/,bQ}$kvi{,8`,_p,6HTH:/X+vjr@l)3XIHi+V|xKvrKR{lT');
define('NONCE_SALT',       'b@c0u1E3l^%os``#Fy4EWB}4j^(#tEiQek*AV7+QA?IM7,L<xRq=8b-iX:7`>_6p');

/**#@-*/

/**
* WordPress Database Table prefix.
*
* You can have multiple installations in one database if you give each a unique
* prefix. Only numbers, letters, and underscores please!
*/
$table_prefix = '85F226TK_';

/**
* WordPress Localized Language, defaults to English.
*
* Change this to localize WordPress. A corresponding MO file for the chosen
* language must be installed to wp-content/languages. For example, install
* de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
* language support.
*/
define('WPLANG', '');

/** set uploads directory */
define( 'UPLOADS', ''.'assets' );

/** manage revisions */
define('AUTOSAVE_INTERVAL', 300 ); // Default value is 60 seconds.
define('WP_POST_REVISIONS', 3); // Number of revisions to save.

/* Media Trash. */
define( 'MEDIA_TRASH', true );
define( 'EMPTY_TRASH_DAYS', '15' );

/* Compression */
//define( 'COMPRESS_CSS',        true );
//define( 'COMPRESS_SCRIPTS',    true );
//define( 'CONCATENATE_SCRIPTS', true );
define( 'ENFORCE_GZIP',        true );

/** jetpack development mode */
define( 'JETPACK_DEV_DEBUG', true);

/**
* For developers: WordPress debugging mode.
*
* Change this to true to enable the display of notices during development.
* It is strongly recommended that plugin and theme developers use WP_DEBUG
* in their development environments.
*/
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
