<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Exhibit Design, display technology, trade show displays | xibita | Home</title>
	<meta name="description" content="">
	<meta name="keywords" content="">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "home"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>

	<!-- Homepage intro -->
	<div id="intro-home" class="row">
		<div class="eight columns centered">
			<h1>Exhibit design, environments, trade show displays and graphics that generate results.</h1>
		</div>
	</div>
 
	<!-- Slider -->
	<div class="row">
		<div class="ten">
			<div id="slider" class="flexslider">
				<ul class="slides">
					<li>
						<img src="/images/banner-home-01.jpg" alt="banner photo" title="" />
						<div class="slidecaptionwrap">
							<div class="slidecaption">
								<p>You have one opportunity to make a lasting impression.</p>
								<p><span>Xibita will ensure it's perfectly executed.</span></p>
							</div>
						</div>
					</li>
					<li>
						<img src="/images/banner-home-02.jpg" alt="banner photo" title="" />
						<div class="slidecaptionwrap">
							<div class="slidecaption">
								<p>Attract better prospects.</p>
								<p><span>Drive interest with portable display products.</span></p>
							</div>
						</div>
					</li>
					<li>
						<img src="/images/banner-home-03.jpg" alt="banner photo" title="" />
						<div class="slidecaptionwrap">
							<div class="slidecaption">
								<p>Full service approach.</p>
								<p><span>We support you through the entire process.</span></p>
							</div>
						</div>
					</li>
					<li>
						<img src="/images/banner-home-04.jpg" alt="banner photo" title="" />
						<div class="slidecaptionwrap">
							<div class="slidecaption">
								<p>Retail and corporate environments redefined.</p>
								<p><span>Xibita can help create the right experiences.</span></p>
							</div>
						</div>
					</li>
					<li>
						<img src="/images/banner-home-05.jpg" alt="banner photo" title="" />
						<div class="slidecaptionwrap">
							<div class="slidecaption">
								<p>As your business grows, so does your portable display requirements.</p>
								<p><span>Stand out from the crowd with a display that is unique and durable.</span></p>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<?php include("includes/slides-portables.php"); ?>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="four columns offset-by-two small-6">
			<h2>Attracting and connecting with the right audience is challenging. <strong>Xibita creates solutions.</strong></h2>
			<p class="subheading">For over 30 years, Xibita has been designing and producing innovative custom environments and portable trade show displays.</p>
			<p>We offer exhibit design, trade show displays manufacturing, exhibit rentals, large-format digital printing, and show services for every aspect of your event. If space is an issue, let us store your exhibit in one of our temperature controlled warehouse facilities to protect and maintain your investment.</p>
			<p>Focused on your growth, we have <a href="locations.php">seven locations</a> across Canada to provide award-winning exhibit solutions that perfectly suit your needs.</p>
			<p>Our team works closely with agencies, businesses and organizations to identify the direction of the project, and develop an action plan to exceed expectations.</p>
			<p>Our passion is to develop, design, and deliver exceptional experiences that connect your brand with the right audience.</p>
			<p>You have one opportunity to make a lasting impression.</p>
			<p>Xibita will ensure it&rsquo;s perfectly executed.</p>
		</div>
		<div class="two columns small-6-home">
			<h3>Why partner with Xibita?</h3>
			<h4>Effective Results.</h4>
			<p>Reduce time and cost associated with locating and buying display and graphic solutions over the lifetime of the product.</p>
			<h4>In-House Manufacturing.</h4>
			<p>All items are produced in-house to ensure the highest standards of quality and timeliness. Repairing and adjusting products is easy and cost-effective.</p>
			<h4>Expertise.</h4>
			<p>We support you. By constantly educating, innovating and evaluating, we focus on sharing our knowledge and increasing the value of your investment.</p>
			<h4>Guaranteed Products.</h4>
			<p>Our product guarantee means that if any problems occur due to the workmanship and/or materials, we will correct the issue. No questions asked!</p>
 			<h4>Full Service Approach.</h4>
			<p>Our <a href="team.php">team of specialists</a> will assist you with the assembly, disassembly, storage and staff training for your display.</p>
		</div>
		<div class="two columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#slider').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				slideshow: true,
				slideshowSpeed: 5000
			});
		});
	</script>
  
</body>
</html>
