<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li class="slide">
						<div id="prjbanners1" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-banners-01.jpg" alt="Modular systems slide" title="">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjbanners2" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-banners-02.jpg" alt="Modular systems slide" title="">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjbanners3" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-banners-03.jpg" alt="Modular systems slide" title="">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjbanners4" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-banners-04.jpg" alt="Modular systems slide" title="">
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div id="portfolionav" class="flexslider">
				<ul class="slides">
					<li>
						<img src="/images/portables/slide-banners-01-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-banners-02-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-banners-03-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-banners-04-t.jpg" alt="" title="">
					</li>
				</ul>
			</div>
		</div>
	</div>