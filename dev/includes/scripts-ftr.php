<script src="/javascripts/foundation.min.js"></script>
	<script src="/javascripts/jquery.flexslider-min.js"></script>
	<script src="/javascripts/jquery.collapse.js"></script>
	<script src="/javascripts/jquery.collapse_storage.js"></script>
	<script src="/javascripts/jquery.collapse_cookie_storage.js"></script>

	<script>
		var _gaq=[['_setAccount','UA-36199829-1'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>
	<script>
	  new jQueryCollapse($("#prefooter"), {
		  open: function() {
			this.slideDown(150);
		},
		close: function() {
			this.slideUp(150);
		}
	  });
	</script>