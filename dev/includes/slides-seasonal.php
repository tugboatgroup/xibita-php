<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li class="slide">
						<div id="prjaccessories1" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-accessories-01.jpg" alt="Seasonal products & accessories slide" title="">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjaccessories2" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-accessories-02.jpg" alt="Seasonal products & accessories slide" title="">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjaccessories3" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-accessories-03.jpg" alt="Seasonal products & accessories slide" title="">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjaccessories4" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-accessories-04.jpg" alt="Seasonal products & accessories slide" title="">
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div id="portfolionav" class="flexslider">
				<ul class="slides">
					<li>
						<img src="/images/portables/slide-accessories-01-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-accessories-02-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-accessories-03-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-accessories-04-t.jpg" alt="" title="">
					</li>
				</ul>
			</div>
		</div>
	</div>