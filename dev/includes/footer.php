	<div id="prefooter" class="show-hide">
		<h3 class="pftr-heading">Join our conversation</h3>
		<div class="pftr-content row">
			<div class="four columns centered">
				<div id="" class="pftr-widget">
					<div class="pftr-widget-header cf">
						<div class="pftr-widget-type"><a href="https://twitter.com/Xibita1" target="_blank">Twitter</a></div>
						<div class="pftr-widget-heading"><h5><a href="https://twitter.com/Xibita1" target="_blank">Follow us @Xibita1</a></h5></div>
					</div>
					<div class="pftr-widget-content cf">
						<div class="my_tweets_block">
							<?php include("includes/tweets.php"); ?>
						</div>
					</div>
				</div>
				<div id="" class="pftr-widget">
					<div class="pftr-widget-header cf">
						<div class="pftr-widget-type"><a href="https://www.facebook.com/pages/Xibita/171499779675659" target="_blank">Facebook</a></div>
						<div class="pftr-widget-heading"><h5><a href="https://www.facebook.com/pages/Xibita/171499779675659" target="_blank">Like Us</a></h5></div>
					</div>
					<div class="pftr-widget-content cf">
						<p>Like us for exclusive content about upcoming product lines and further insights about tradeshow displays.</p>
					</div>
				</div>
				<div id="" class="pftr-widget">
					<div class="pftr-widget-header cf">
						<div class="pftr-widget-type"><a href="http://instagram.com/xibitaca" target="_blank">Instagram</a></div>
						<div class="pftr-widget-heading"><h5><a href="http://instagram.com/xibitaca" target="_blank">Follow us @Xibitaca</a></h5></div>
					</div>
					<div class="pftr-widget-content cf">
						<div class="instagram"></div>
						<script type="text/javascript">
							var accessToken = '471646092.4093d8f.11b3fa658c32441b935d00d1ac9c8153';
							var userId = 471646092;
							jQuery(".instagram").instagram({
								userId: userId,
								accessToken: accessToken,
								show: 3
							});
						</script>
					</div>
				</div>
				<div id="" class="pftr-widget">
					<div class="pftr-widget-header cf">
						<div class="pftr-widget-type"><a href="http://pinterest.com/xibitacanada/" target="_blank">Pinterest</a></div>
						<div class="pftr-widget-heading"><h5><a href="http://pinterest.com/xibitacanada/" target="_blank">Follow us</a></h5></div>
					</div>
					<div class="pftr-widget-content cf">
						<p>Follow us on Pintrest for exhibit design inspiration.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<div class="row">
			<div class="ten columns">
				<div class="row">
					<div class="eight columns centered">
						<h6>Contact Us</h6>
						<ul id="ftr-contacts">
							<li class="headoffice"><p><strong>Vancouver</strong><br><span>Head Office</span><br>109 - 3551 Viking Way<br>Richmond, BC<br>V6V 1W1</p><p><strong>T</strong> (604) 276-2366<br><strong>F</strong> (604) 276-0860<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
							<li><p><strong>Victoria</strong><br>108 - 1027 Pandora Ave<br>Victoria, BC<br>V8V 3P6</p><p><strong>T</strong> (250) 413-3124<br><strong>F</strong> (604) 276-0860<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
							<li><p><strong>Calgary</strong><br>10 - 2015 32nd Ave NE<br>Calgary, AB<br>T2E 6Z3</p><p><strong>T</strong> (403) 261-5980<br><strong>F</strong> (403) 261-5998<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
							<li><p><strong>Toronto</strong><br>710 Gordon Baker Rd<br>Toronto, ON<br>M2H 3B4</p><p><strong>T</strong> (416) 494-9553<br><strong>F</strong> (416) 494-7462<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
							<li><p><strong>Cambridge</strong><br>6D - 25 Struck Court<br>Cambirdge, ON<br>N1R 8L2</p><p><strong>T</strong> (519) 623-2989<br><strong>F</strong> (519) 623-1714<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
							<li><p><strong>Ottawa</strong><br>9 - 190 Colonnade Rd S.<br>Ottawa, ON<br>K2E 7J5</p><p><strong>T</strong> (613) 723-1849<br><strong>F</strong> (613) 723-5489<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
							<li><p><strong>Montreal</strong><br>309 - 1850 Boul.<br>Le Corbusier<br>Laval, QC<br>H7S 2K1</p><p><strong>T</strong> (514) 832-0694<br><strong>F</strong> (416) 494-7462<br><a href="mailto:ideas@xibita.com">ideas@xibita.com</a></p></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="eight columns centered">
						<div class="credit"><a href="http://www.creative-engine.ca/" target="_blank">Design by: <strong>Creative Engine</strong></a></div>
					</div>
				</div>
			</div>
		</div>
	</footer>