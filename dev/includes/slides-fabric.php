<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li class="slide">
						<div id="prjfabric1" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-fabric-01.jpg" alt="Modular systems slide" title="">
								</li>
								<li class="projectslide">
									<img src="/images/portables/slide-fabric-01b.jpg" alt="Modular systems slide" title="">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjfabric2" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-fabric-02.jpg" alt="Modular systems slide" title="">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjfabric3" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-fabric-03.jpg" alt="Modular systems slide" title="">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjfabric4" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-fabric-04.jpg" alt="Modular systems slide" title="">
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div id="portfolionav" class="flexslider">
				<ul class="slides">
					<li>
						<img src="/images/portables/slide-fabric-01-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-fabric-02-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-fabric-03-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-fabric-04-t.jpg" alt="" title="">
					</li>
				</ul>
			</div>
		</div>
	</div>