<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Modular display, modular display booth, booth displays| xibita | Modular Display Stands</title>
	<meta name="description" content="Xibita offers modular display booth solutions that are flexible to meet your needs.">
	<meta name="keywords" content="modular display, modular display booth, booth displays, exhibit displays, trade show display booth, tradeshow booth, booth display, trade show booth">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<?php include("includes/slides-modular.php"); ?>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Modular display systems &ndash; ready to grow.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a></li>
				<li class="active"><a href="/modular-systems">Modular Systems</a></li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">As your business grows, modular display systems offer the flexibility to grow with you.</p>
			<p>With integrated workstations, product displays and merchandise storage, modular exhibit systems give you a platform to present information, give demonstrations and capture lead information.</p>
			<p>At Xibita we respect budgets, which is why we design all our modular systems to grow as budgets become available and requirements adjust. Start with one banner stand and as your business needs warrant, add modular components to your exhibit solution. Xibita will be there to support you at every stage of your exhibit program.</p>
			<div class="show-hide">
				<div class="accordionwrap">
					<h2 class="open"><img src="images/logo-modular-network.png" width="128" height="29" alt="Network Modular System" title="Network and Network Table Top Modular System"></h2>
					<div class="content">
						<h3>Image is everything&hellip; especially at your next event or trade show. With smooth curves and seamless presentation, the NETWORK display line is hard to miss.</h3>
						<p>Made of durable anodized aircraft aluminum, this NETWORK modular display system offers rugged good looks and is virtually impossible to break with normal use. Its lightweight strength and rigidity is unmatched in the industry.</p>
						<p>The NETWORK display frame collapses neatly into a single portable All-In-One Case and transforms into an attractive Podium to create a dramatic presence and add impact to your overall presentation.</p>
						<p>Designed to be expanded, reduced or reconfigured without tools or tinkering. Change your trade show display from a flat wall to a curve or an S-curve. Or, build it into a unique arch that towers above.  NETWORK is portability combined with the flexibility you need.</p>
						<h3>Table Top Trade Show Display Series</h3>
						<p>If space is of a premium for you, the NETWORK table top series provides a modular solution that can be expanded into a full-size floor standing exhibit as your business grows.</p>
						<p>Each system offers an attractive backdrop and supports any of our three panels:</p>
						<ul>
							<li>Fabric</li>
							<li>Designer finish</li>
							<li>Printed graphic</li>
						</ul>
						<h3>Promotion</h3>
						<h4>Refresh and Upgrade your NETWORK Booth Today!</h4>
						<p>The NETWORK Pop-up display system is the most versatile system on the market. NETWORK&rsquo;s modularity allows it to be expanded, reduced or reconfigured without tools. You can add inches to the height or extra sections to the width!</p>
						<p>Are your NETWORK graphics getting stale?<br>
						Do you want to transform your display instantly?</p>
						<p>Give your 3x3 NETWORK booth a facelift without incurring the cost of a complete makeover. When you need to change your graphic message, allow us to upgrade your booth to a 4x3 size by adding a 6th digital panel, hardware and extra universal infill bars. Adopt the latest flat end caps for a more modern look.</p>
						<p>Not only should you feel good about helping the environment by reusing your existing hardware, your budget will embrace this offer.</p>
						<p>To learn more about how you can refresh your NETWORK Pop-Up Display System <a href="mailto:ideas@xibita.com">contact us</a> today.</p>
						<img src="images/products/network.jpg" width="494" height="309" alt="network">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-modular-satellite.png" width="85" height="29" alt="Satellite Modular System" title="Satellite Modular System"></h2>
					<div class="content">
						<h3>Command attention in any room; communicate from any angle.</h3>
						<p>From top to bottom, side to side, the SATELLITE&rsquo;s strong and sturdy presence commands attention, while its elegant appearance invites admiration. It handles itself well in any room and is ready to make a statement at a moment's notice.</p>
						<p>Its removable seamless graphic panels can be dressed to fit each occasion and the structure itself can be used as a key architectural component in large-scale exhibits.</p>
						<p>The SATELLITE is easy to set up in minutes and take down just as fast. Expand the lightweight frames, lock them into place and connect the panels. When you're done, take it down and pack it away in its nylon carrying case - another example of power through simplicity. Available in a 3-section tower or a 4-section high-rise to meet your specific communication needs.</p>
						<img src="images/products/satellite.jpg" width="494" height="309" alt="satellite">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-modular-original8.png" width="73" height="35" alt="Original 8 Modular System" title="Original 8 Modular System"></h2>
					<div class="content">
						<h3>Durable and expandable, ORIGINAL 8 can change when your needs do.</h3>
						<p>ORIGINAL 8 is made of durable anodized aircraft aluminum. Its strong rectangular frames make the system almost impossible to damage with normal use. And with its patented 360 degree hinges and auto-locking device, you can expand, reduce or reconfigure ORIGINAL 8 as you wish.</p>
						<p>Each ORIGINAL 8 frame features twin channels that support both a front and back graphic panel for double the impact. Replacing graphic panels for a new look or promotion is easy and effortless with simple ball bearing release openings.</p>
						<p>The standard 10-foot exhibit compacts into a nylon carrying case that tucks neatly in the trunk of most cars. Once at the show just unpack, unfold and display a full-size exhibit in seconds!</p>
						<h3>Table Top Trade Show Display Series</h3>
						<p>If space is of a premium for you, the Original 8 table top series provides a modular smaller-scale solution that can also be expanded into a full-size floor standing exhibit should your needs change.</p>
						<p>Each system offers an attractive backdrop and supports any of our three panels:</p>
						<ul>
							<li>Fabric</li>
							<li>Designer finish</li>
							<li>Printed graphic</li>
						</ul>
						<img src="images/products/original-8.jpg" width="494" height="309" alt="original-8">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-modular-algo.png" width="73" height="22" alt="ALGO Modular System" title="ALGO Modular System"></h2>
					<div class="content">
						<h3>Looking for an expandable trade show display that offers both counters and podiums? Algo can meet your trade show booth requirements.</h3>
						<p>When your exhibit needs require solid structures, but laminated wood is not the right solution - ALGO is the answer. ALGO is Xibita&rsquo;s structural, lightweight extrusion frame system.</p>
						<p>Whether you are looking to build 14-foot towers or simple counters and pedestals, ALGO provides the building blocks for the perfect modular display system.</p>
						<p>Even though ALGO is slim in size, it delivers beyond its weight.  Designed to address the heavy requirements of a busy tradeshow schedule, ALGO comes with a lifetime warranty against manufacturer&rsquo;s defects.</p>
						<img src="images/products/algo.jpg" width="494" height="309" alt="algo">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
			</div>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21,
				asNavFor: '#portfolio'
			});

			$('#portfolio').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav:false,
				selector: ".slides > .slide",
				sync: "#portfolionav"
			});

			$('#prjmodularcma').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjmodular1').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjmodular2').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjmodular3').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjmodular4').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});
		});
	</script>
	<script>
	  new jQueryCollapse($(".show-hide"), {
		 query: 'div h2',
		 open: function() {
			this.slideDown(150);
		 },
		 close: function() {
			this.slideUp(150);
		 }
	  });
	</script>
  
</body>
</html>
