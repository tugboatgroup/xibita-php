<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Display banners, portable displays, tradeshow displays | xibita | Fabric Portable Exhibits</title>
	<meta name="description" content="Portable displays that use fabric are easy to maintain and easy to set up. Xibita can create a custom solution that is right for you.">
	<meta name="keywords" content="fabric display, fabric displays, portable fabric banner, fabric tradeshow banner, fabric tradeshow banners">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<?php include("includes/slides-fabric.php"); ?>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Visually stunning and easy to use. Xibita&rsquo;s fabric system exhibits are eye-catching.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li class="active"><a href="/fabric-systems">Fabric Systems</a></li>
				<li><a href="/modular-systems">Modular Systems</a></li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">Versatile and flexible, Fabric Exhibit Structures demand attention.</p>
			<p>With a multitude of shapes and forms, versatility is a cornerstone of Xibita&rsquo;s Fabric System exhibits.</p>
			<p>To create a TENSION FABRIC structure, your image is heat-pressed from transfer paper onto fabric with stretch qualities similar to Lycra&reg;.  The high-quality fabric and process results in a display that is easy to assemble and maintain, and crisp images to share your message.</p>
			<div class="show-hide">
				<div class="accordionwrap">
					<h2 class="open"><img src="images/logo-kado.png" width="135" height="25" alt="KADO Fabric System" title="KADO Fabric System"></h2>
					<div class="content">
						<h3>KADO fabric displays offer the best modular approach for your display. Customizable and durable, the KADO product line can meet all of your exhibit and trade show display requirements.</h3>
						<p>The KADO product line combines affordability and customization that tests the boundaries of fabric structures. Limitless solutions are built into KADO.</p>
						<p>Made from 1&frac14;&rdquo; to 2&rdquo;aluminum pipe, this durable tension fabric display system has the ability to grow alongside your exhibit requirements. Build towers, counters or kiosks - it&rsquo;s the perfect product to &ldquo;mix and match,&rdquo; so your initial investment is always protected.</p>
						<img src="images/products/kado.jpg" width="494" height="309" alt="kado">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-yello.png" width="135" height="25" alt="YELLO Fabric System" title="YELLO Fabric System"></h2>
					<div class="content">
						<h3>The YELLO modular display can address your tradeshow display needs. With a one-year frame warranty, you can trust the quality of the YELLO fabric tradeshow system.</h3>
						<p>If you are looking for a simple system that addresses your tradeshow requirements, YELLO is the ideal solution.</p>
						<p>Built to withstand the rigors of your show schedule, the YELLO fabric tradeshow system is constructed around a graphic back wall or hanging sign. It&rsquo;s easy to set-up, has lightweight construction, and is available in a variety of sizes and configurations.</p>
						<img src="images/products/yello.jpg" width="494" height="309" alt="yello">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-exhale.png" width="135" height="25" alt="EXHALE Fabric System" title="EXHALE Fabric System"></h2>
					<div class="content">
						<h3>Xibita&rsquo;s EXHALE fabric exhibit system is easy as 1, 2, 3. Pop it. Lock it. And it&rsquo;s done. Set up your display quickly and easily with the EXHALE FABRIC POP-UP display system.</h3>
						<p>The EXHALE FABRIC POP-UP display features a durable fabric that is attached to the scissor network and stretches smooth once unfolded. When you are ready to exhibit, simply pop open EXHALE, and lock the frame into place.</p>
						<p>EXHALE is designed to be lightweight and easily transportable. There is no reason to worry about airline weight restrictions when taking your display on the road.</p>
						<img src="images/products/exhale.jpg" width="494" height="309" alt="exhale">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-kango.png" width="135" height="25" alt="KANGO Fabric System" title="KANGO Fabric System"></h2>
					<div class="content">
						<h3>Mix-n-match, change as you go, KANGO offers the ultimate in flexibility.</h3>
						<p>KANGO is the ultimate P.O.S. Display. With a leaning towards flexibility and a limitless array of configurations, you literally can have a different look every time you hit the show floor.</p>
						<p>KANGO changeable fabric graphics go on without any tools and remain on the display for easy set-up and take-down. KANGO allows you to create a 3-D environment with front and back graphics that allows for stunning visual impact. Take the opportunity to place a shelf on one of the sections and KANGO not only highlights your message but also shows off your lighter weight products.</p>
						<img src="images/products/kango.jpg" width="232" height="309" alt="kango">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-hype.png" width="135" height="25" alt="HYPE Fabric System" title="HYPE Fabric System"></h2>
					<div class="content">
						<h3>When a graphic wall is needed HYPE is perfectly poised to perform.</h3>
						<p>HYPE is as simple as it looks. 4 poles held together securely by a clutch lock system that clicks into place and is held upright by steel &ldquo;bridge&rdquo; feet. Substantial design and a high-quality graphic, create a lasting impression whilst respecting your budget.</p>
						<img src="images/products/hype.jpg" width="494" height="309" alt="hype">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
			</div>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				directionNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21,
				asNavFor: '#portfolio'
			});

			$('#portfolio').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav:false,
				selector: ".slides > .slide",
				sync: "#portfolionav"
			});

			$('#prjfabric1').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjfabric2').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjfabric3').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjfabric4').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});
		});
	
	  new jQueryCollapse($(".show-hide"), {
		 query: 'div h2',
		 open: function() {
			this.slideDown(150);
		 },
		 close: function() {
			this.slideUp(150);
		 }
	  });
	</script>
  
</body>
</html>
