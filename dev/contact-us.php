<?php session_start(); ?>
<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Your Exhibit and Trade Show Specialists | xibita | Contact us</title>
	<meta name="description" content="Call us to discuss your upcoming trade show, event or your work environment.  We are here to help.">
	<meta name="keywords" content="trade show event, tradeshow productions, display solutions, display solution, custom display, display company, custom graphic design services">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "contact"; ?>
	<?php
		if (!empty($_POST['submit-question'])) {
			$qerror = "";

			if (!empty($_POST['qname'])) {
				$qname = $_POST['qname'];
			} else {
				$qerror .= "You didn't provide your name. <br>";
			}

			if (!empty($_POST['qemail'])) {
				$qemail = $_POST['qemail'];
  				if (!preg_match("/^[a-z0-9]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $qemail)){ 
  					$qerror .= "The e-mail address you entered is not valid. <br>";
  				}
			} else {
				$qerror .= "You didn't provide an e-mail address. <br>";
			}

			if (!empty($_POST['qmessage'])) {
				$qmessage = $_POST['qmessage'];
			} else {
				$qerror .= "You didn't include a question/message. <br>";
			}

			if(($_POST['qcode']) == $_SESSION['qcode']) { 
				$qcode = $_POST['qcode'];
			} else { 
				$qerror .= "The captcha code you entered does not match. Please try again. <br>";    
			}

			if (empty($qerror)) {
				$from = 'From: ' . $qname . ' <' . $qemail . '>';
				$to = "ideas@xibita.com";
				$subject = "New contact form message";
				$content = $qname . " has sent you a message: \n" . $qmessage;
				$qsuccess = "<h3>Thank you! Your message has been sent!</h3>";
				mail($to,$subject,$content,$from);
			}
		}
		
		if (!empty($_POST['submit-quote'])) {
			$perror = "";

			if (!empty($_POST['pname'])) {
				$pname = $_POST['pname'];
			} else {
				$perror .= "You didn't provide your name. <br>";
			}

			if (!empty($_POST['pemail'])) {
				$pemail = $_POST['pemail'];
  				if (!preg_match("/^[a-z0-9]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $pemail)){ 
  					$perror .= "The e-mail address you entered is not valid. <br>";
  				}
			} else {
				$perror .= "You didn't provide an e-mail address. <br>";
			}

			if (!empty($_POST['pscope'])) {
				$pscope = $_POST['pscope'];
			} else {
				$perror .= "You didn't indicate the scope of work. <br>";
			}

			if (!empty($_POST['pbudget'])) {
				$pbudget = $_POST['pbudget'];
			} else {
				$perror .= "You didn't provide a project budget. <br>";
			}

			if (!empty($_POST['ptiming'])) {
				$ptiming = $_POST['ptiming'];
			} else {
				$perror .= "You didn't provide project timing. <br>";
			}

			if (!empty($_POST['pdescription'])) {
				$pdescription = $_POST['pdescription'];
			} else {
				$perror .= "You didn't provide a project description. <br>";
			}

			if(($_POST['pcode']) == $_SESSION['pcode']) { 
				$pcode = $_POST['pcode'];
			} else { 
				$perror .= "The captcha code you entered does not match. Please try again. <br>";    
			}

			if (empty($perror)) {
				$from = 'From: ' . $pname . ' <' . $pemail . '>';
				$to = "ideas@xibita.com";
				$subject = "New quote request";
				$content = $pname . " has sent you a quote request: \n Scope of work:" . $pscope . "\n Budget: " . $pbudget . "\n Timing: " . $ptiming . "\n Brief project description: " . $pdescription;
				$psuccess = "<h3>Thank you! Your quote request has been sent!</h3>";
				mail($to,$subject,$content,$from);
			}
		}
	?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="slider">
			<img src="/images/banner-contact.jpg" alt="banner photo" title="Contact Xibita" />
			</div>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>How may we help you?</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li class="active"><a href="/contact-us">Contact Us</a></li>
				<li><a href="/locations">Locations</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">Xibita creates exhibit design and trade show displays using state of the art display technology to improve your branding outcomes.</p>
			<p>Our mission is simple: to help your message stand out from your competition.</p>
			<p>Are you ready to discuss your exhibit, environment or tradeshow requirements?</p>
			<p>Please select your preferred communication method (email and customer service request response time is one business day or less):</p>
			<ul>
				<li><a href="/locations">Find a toll free number and call us</a></li>
				<li><a href="#quote">Submit a customer service project request</a></li>
				<li><a href="#question">Email us with general inquiries</a></li>
			</ul>
			<h2 id="question">Have a specific question?</h2>
			<p>Send your comments or questions by using the form below.</p>
			<div class="form cf">
				<?php
					if (!empty($qerror)) {
						echo '<p class="error"><strong>Your question was NOT sent<br> The following error(s) returned:</strong><br>' . $qerror . '</p>';
					} elseif (!empty($qsuccess)) {
						echo $qsuccess;
					}
				?>
				<form action="contact-us.php" method="post">
					<label>Name:</label>
					<input type="text" name="qname" value="<?php if($_POST['qname']) { echo $_POST['qname']; } ?>">
					<label>Email:</label>
					<input type="text" name="qemail" value="<?php if($_POST['qemail']) { echo $_POST['qemail']; } ?>">
					<label>Message:</label>
					<textarea name="qmessage" rows="20" cols="20"><?php if($_POST['qmessage']) { echo $_POST['qmessage']; } ?></textarea>
					<label>Enter the code in the box below: <img src="/includes/captcha-question.php"></label>
					<input type="text" name="qcode" class="captcha">
					<input type="submit" class="submit" name="submit-question" value="Ask Xibita">
				</form>
			</div>
			
			<h2 id="quote">Request a Quote</h2>
			<div class="form cf">
				<?php
					if (!empty($perror)) {
						echo '<p class="error"><strong>Your quote request was NOT sent<br> The following error(s) returned:</strong><br>' . $perror . '</p>';
					} elseif (!empty($psuccess)) {
						echo $psuccess;
					}
				?>
				<form action="contact-us.php" method="post">
					<label>Name:</label>
					<input type="text" name="pname" value="<?php if($_POST['pname']) { echo $_POST['pname']; } ?>">
					<label>Email:</label>
					<input type="text" name="pemail" value="<?php if($_POST['pemail']) { echo $_POST['pemail']; } ?>">
					<label>Scope of Work:</label>
					<textarea name="pscope" rows="20" cols="20"><?php if($_POST['pscope']) { echo $_POST['pscope']; } ?></textarea>
					<label>Budget:</label>
					<input type="text" name="pbudget" value="<?php if($_POST['pbudget']) { echo $_POST['pbudget']; } ?>">
					<label>Timing:</label>
					<input type="text" name="ptiming" value="<?php if($_POST['ptiming']) { echo $_POST['ptiming']; } ?>">
					<label>Brief Project Description:</label>
					<textarea name="pdescription" rows="20" cols="20"><?php if($_POST['pdescription']) { echo $_POST['pdescription']; } ?></textarea>
					<label>Enter the code in the box below: <img src="/includes/captcha-quote.php"></label>
					<input type="text" name="pcode" class="captcha"> 
					<input type="submit" class="submit" name="submit-quote" value="Request Quote">
				</form>
			</div>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').orbit({
					timer:false
				});
		});
	</script>
  
</body>
</html>
