<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Tradeshow exhibits, exhibit display, tradeshow services | xibita | Karen Hansler</title>
	<meta name="description" content="Xibita has a leadership team that offers insight, expertise and assistance for your next tradeshow event.">
	<meta name="keywords" content="trade show exhibits, display booths or trade show design, trade show display, trade show exhibits, banner stands, trade show designs, display booth, digital printing, booth display, booth displays">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "about"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="slider">
			<img src="/images/banner-team.jpg" />
			</div>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Xibita's Leadership Team</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/about">About Us</a></li>
				<li class="forceborder"><a href="/team">People</a>
					<ul>
						<li><a href="/people/hanif-muljiani">Hanif Muljiani</a></li>
						<li><a href="/people/ken-button">Ken Button</a></li>
						<li><a href="/people/marie-rowley">Marie Rowley</a></li>
						<li><a href="/people/mike-cooper">Mike Cooper</a></li>
						<li><a href="/people/declan-withers">Declan Withers</a></li>
						<li><a href="/people/sue-ivanic">Sue Ivanic</a></li>
						<li class="active"><a href="/people/karen-hansler">Karen Hansler</a></li>
						<li><a href="/people/james-ayotte">James Ayotte</a></li>
						<li><a href="/people/josh-green">Josh Green</a></li>
						<li><a href="/people/aubin-symons">Aubin Symons</a></li>
					</ul>
				</li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<h2><strong>Karen Hansler</strong> <span>Director of Creative Services</span></h2>
			<p class="subheading">At Xibita, creativity is based on connecting the right ideas to solve exhibit and display challenges.</p>
			<p>Karen Hansler, Director of Creative Services, is driven to uncover limitless possibilities for each display and environment. She both challenges and pushes creative boundaries for her clients, always effectively communicating throughout the design process.</p>
			<p>With a 17-year exhibit design background, both graphically and structurally, Karen&rsquo;s prior experience as a senior graphic designer adds depth and insight. Karen&rsquo;s passion is seeing Xibita&rsquo;s concepts &ndash; from design to installation - come to life on the tradeshow floor.</p>
			<p>Karen graduated from the Ontario College of Art & Design with a major in commercial design and fine arts.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').orbit({
					timer:false
				});
		});
	</script>
  
</body>
</html>
