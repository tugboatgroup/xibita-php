<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Roll Up Banner stands, retractable banners, portable displays | xibita | Roll Up Banners and Retractable Banners</title>
	<meta name="description" content="Looking for a light weight portable solution?  Xibita offers a variety of roll up banner stand and retractable portable displays to meet your requirements.">
	<meta name="keywords" content="retractable banners, retractable banner stands, portable banner stands, best portable banner">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<?php include("includes/slides-banners.php"); ?>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Xibita's roll-up and retractable banner lines can improve your results.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li class="active"><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a></li>
				<li><a href="/modular-systems">Modular Systems</a></li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">Trade show and Retail Displays that get noticed.</p>
			<p>Eye-catching, economical and easy to set up, our portable roll-up banner stands take the fuss out of the preparation and delivery of your message. From roll-up banners to retractable cassettes, we offer every type of banner stand to meet your individual needs.</p>
			<p>Roll-up and retractable banner stands are ideal in free-standing open areas such as malls, showrooms and tradeshows &ndash; and are the answer to capturing the attention of prospects that are coming and going.</p>
			<p>Our lightweight, tall and wide roll-up and retractable banners can be a modular component to a larger display, or can help promote &ldquo;in store&rdquo; specials and promotions. Working together, two or more banners can provide additional impact to your message.</p>
			<h2>Retractable Banner Stands</h2>
			<p>Xibita&rsquo;s retractable banner lines are designed to make a big impression at a moment's notice.</p>
			<p>With an engineered retractable cassette as its base, retractables set-up instantly by lifting and securing the banner in place. After the event, the banner is stored and protected in the cassette until called to action.</p>
			<p>Looking to change your banner? Our graphics can be replaced as often as you need &ndash; making our systems reuseable for many years to come.</p>
			<div class="show-hide">
				<div class="accordionwrap">
					<h2 class="open"><img src="images/logo-zap.png" width="135" height="25" alt="ZAP Retractable Banner Stand" title="ZAP Retractable Banner Stand"></h2>
					<div class="content">
						<h3>Created with simplicity in mind, ZAP assembles in seconds with results that are truly impressive.</h3>
						<p>Utilizing a unique retractable cassette as its base, ZAP maintains and consistently showcases your message over and over. ZAP banner stands are ideal for free-standing open areas such as malls, showrooms and tradeshows where you can capture the attention of prospects that are coming and going.</p>
						<p>Tall, wide yet light. ZAP can be adjusted in height from a petite 35 inches to a handsomely tall 88 inches. And yet its 35 inch-wide base, including telescoping pole, banner and nylon carrying case weighs remarkably less than 12 lbs!</p>
						<img src="images/products/zap.jpg" width="494" height="309" alt="zap">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-roso.png" width="135" height="25" alt="ROSO Retractable Banner Stand" title="ROSO Retractable Banner Stand"></h2>
					<div class="content">
						<h3>ROSO's retractable banner is both light-weight and durable enough for everyday use.</h3>
						<p>Use ROSO as a permanent display or take it on the road! Toss a ROSO in the back of a car or check it in as luggage. ROSO's sturdy padded nylon carrying case fully protects your investment.</p>
						<p>Affordable: ROSO retractable banner stand is powerful enough to stand independently. But why not take advantage of its deceptively attractive price point and share your message in multiples? Highlight your entire product line or suite of services, display in multiple locations or support an entire dealer or franchise network. Count on ROSO to carry your brand message everywhere!</p>
						<img src="images/products/roso.jpg" width="494" height="309" alt="roso">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
			</div>
			<br>
			<br>
			<h2>Roll-Up Banner Stands</h2>
			<p>Cost-effective and reliable, our roll-up banners offer a strong and stable solution that is easy for you to set-up.</p>
			<p>Our lightweight systems are based on three pieces that come together to make an impact. When the show is over, your banner folds down and fits into a nylon carrying case, so you can be on the road anywhere and at any time. With environmental banner options, the roll-up system is an attractive product both to your budget and the environment.</p>
			<p>Roll-up banner systems give you the flexibility of presenting your message without any mechanical systems. Simply add or remove the graphic as your needs dictate. Changing graphics is easy and hassle-free. Without any moving parts, roll-up banner systems are ideal for outdoor requirements.</p>
			<p>Roll-up systems come with telescoping or fixed length options. The ability to double your graphic exposure is also readily available. With a choice of fabric or vinyl graphic options, a vibrant message is easily within the reach of roll-up banner systems.</p>
			<div class="show-hide">
				<div class="accordionwrap">
					<h2><img src="images/logo-solo.png" width="135" height="25" alt="SOLO Retractable Banner Stand" title="SOLO Retractable Banner Stand"></h2>
					<div class="content">
						<h3>SOLO achieves a world-first in combining affordability, durability and sheer quality.</h3>
						<p>Hardworking features without frills. SOLO is a unique roll-up banner stand that does not rely on a cartridge system. The banners are simply rolled and unrolled to the height you require. Due to its roll-up feature SOLO can accommodate a double-sided graphic in each one of its twin-channels. When you need to have two messages but don&rsquo;t want the hassle of two stands, SOLO is the perfect choice.</p>
						<p>SOLO is a strong and stable, yet lightweight 3-piece system that takes less than two minutes to assemble. When the show is over, SOLO simply rolls down and fits into a handy carrying case, along with your rolled-up display banners.</p>
						<img src="images/products/solo.jpg" width="494" height="309" alt="solo">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-econ.png" width="135" height="25" alt="ECON Retractable Banner Stand" title="ECON Retractable Banner Stand"></h2>
					<div class="content">
						<h3>Hook it up, Hang it out.</h3>
						<p>ECON is versatile both indoors and outdoors. With seven banner sizes that all fit on one frame, ECON offers you choice above all. Printed on durable smooth vinyl, you message is perfectly presented to your audience.</p>
						<img src="images/products/econ.jpg" width="494" height="309" alt="econ">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-bambu.png" width="135" height="25" alt="BAMBU Retractable Banner Stand" title="BAMBU Retractable Banner Stand"></h2>
					<div class="content">
						<h3>Saving the planet one message at a time!</h3>
						<p>The BAMBU banner stand is one of the first environmentally-responsible display products on the market. The bamboo frame and graphic panel produced with biodegradable inks makes this one of the most renewable banner stands on earth.</p>
						<p>Sparing no compromise on quality this banner stand is a cost-effective, environmentally responsible product that keeps your budget and conscience in check.</p>
						<img src="images/products/bambu.jpg" width="494" height="309" alt="bambu">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-acrobat.png" width="135" height="25" alt="ACROBAT Retractable Banner Stand" title="ACROBAT Retractable Banner Stand"></h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>Attractive, smart and affordable, our Banner Stands take the fuss out of the preparation and delivery of your message. From roll ups to retractable cassettes, we offer every type of stand to meet your individual needs.</p>
							</div>
							<div class="four columns">
								<img src="images/products/acrobat.jpg" width="232" height="309" alt="Acrobat outdoor banner" title="Acrobat outdoor banner">
							</div>
						</div>
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
				<div class="accordionwrap">
					<h2><img src="images/logo-brio.png" width="135" height="25" alt="BRIO Retractable Banner Stand" title="BRIO Retractable Banner Stand"></h2>
					<div class="content">
						<h3>The perfect size for smaller needs.</h3>
						<p>When a banner stand is too big, BRIO is perfect.</p>
						<p>The BRIO is a tension banner-stand perfect for any table-top message or display that doesn&rsquo;t compromise on quality. Its L-stand frame provides stability and strength with minimum footprint.</p>
						<p>Its aluminum frame provides the right amount tension to keep your message crease-free and stunning.</p>
						<img src="images/products/brio.jpg" width="494" height="309" alt="brio">
						<!-- <div class="downloads cf">
													<ul>
														<li><a href="/docs/file.pdf">Brochure</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Set-up Guide</a> (PDF)</li>
														<li><a href="/docs/file.pdf">Specifications</a> (PDF)</li>
													</ul>
												</div> -->
					</div>
				</div>
			</div>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				directionNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21,
				asNavFor: '#portfolio'
			});

			$('#portfolio').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav:false,
				selector: ".slides > .slide",
				sync: "#portfolionav"
			});

			$('#prjbanners1').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjbanners2').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjbanners3').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjbanners4').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});
		});
	
	  new jQueryCollapse($(".show-hide"), {
		 query: 'div h2',
		 open: function() {
			this.slideDown(150);
		 },
		 close: function() {
			this.slideUp(150);
		 }
	  });
	</script>
  
</body>
</html>
