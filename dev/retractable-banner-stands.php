<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Roll Up Banner stands, retractable banners, portable displays | xibita | Roll Up Banners and Retractable Banners</title>
	<meta name="description" content="Looking for a durable portable display solution?  Xibita offers a variety of retractable portable displays to meet your requirements.">
	<meta name="keywords" content="retractable banners, retractable banner stands, portable retractable banner stands, best retractable banner">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li class="slide">
						<div id="prjfortis" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="http://placehold.it/1018x454&text=[fortis 1]" />
								</li>
								<li class="projectslide">
									<img src="http://placehold.it/1018x454/f9f9f8/666666&text=[fortis 2]" />
								</li>
								<li class="projectslide">
									<img src="http://placehold.it/1018x454/1e0576/ffffff&text=[fortis 3]" />
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjanimas" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="http://placehold.it/1018x454&text=[animas 1]" />
								</li>
								<li class="projectslide">
									<img src="http://placehold.it/1018x454/f9f9f8/666666&text=[animas 2]" />
								</li>
								<li class="projectslide">
									<img src="http://placehold.it/1018x454/1e0576/ffffff&text=[animas 3]" />
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<img src="http://placehold.it/1018x454&text=[project 3]" />
					</li>
					<li class="slide">
						<img src="http://placehold.it/1018x454/1e0576/ffffff&text=[project 4]" />
					</li>
					<li class="slide">
						<img src="http://placehold.it/1018x454&text=[project 5]" />
					</li>
					<li class="slide">
						<img src="http://placehold.it/1018x454/1e0576/ffffff&text=[project 6]" />
					</li>
					<li class="slide">
						<img src="http://placehold.it/1018x454&text=[project 7]" />
					</li>
					<li class="slide">
						<img src="http://placehold.it/1018x454/1e0576/ffffff&text=[project 8]" />
					</li>
				</ul>
			</div>
			<?php include("includes/slides-portables.php"); ?>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Looking for a Lightweight, Durable and Flexible Custom Retractable Display? Xibita&rsquo;s Retractable Banner Lines produce improved results.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a>
					<ul>
						<li class="active"><a href="/retractable-banner-stands">Retractable Banners</a></li>
						<li><a href="/roll-up-stands">Roll-up Stands</a></li>
					</ul>
				</li>
				<li><a href="/fabric-systems">Fabric Systems</a></li>
				<li><a href="/modular-systems">Modular Systems</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">Retractable Trade Show Displays that get noticed.</p>
			<p>Xibita&rsquo;s retractable banner lines are designed to make a big impression at a moment's notice.</p>
			<p>With an engineered retractable cassette as its base, you can get set up instantly by lifting and securing the banner in place.    After the event, the banner is stored and protected in the cassette until you need it again.</p>
			<p>Looking to change your banner?  Our retractable systems can be replaced as often as you need &ndash; making our systems useful and useable for many years to come.</p>
			<h2>The Xibita Difference</h2>
			<ul>
				<li>Fast assembly and disassembly</li>
				<li>Perfect balance between size and weight</li>
				<li>Hassle-free ownership and maintenance</li>
			</ul>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21
			});

			$('#portfolio').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav:false,
				selector: ".slides > .slide"
			});

			$('#prjfortis').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: false
			});

			$('#prjanimas').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: false
			});
		});
	</script>
  
</body>
</html>
