<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Contact us | xibita | Regional Offices</title>
	<meta name="description" content="In order to serve you, Xibita has six offices across Canada to help you with your trade show, event or your work environment.  Call us! We are here to help.">
	<meta name="keywords" content="trade show event, tradeshow productions, display solutions, display solution, custom display, display company, custom graphic design services, Canada, best display company">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "contact"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="slider">
			<img src="/images/banner-locations.jpg" alt="banner photo" title="Xibita's regional locations" />
			</div>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>How may we help you?</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/contact-us">Contact Us</a></li>
				<li class="active"><a href="/locations">Locations</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p>If you have questions about custom displays, exhibits or environments, we are here to help. Contact us at the following locations:</p>
			<p><strong>Vancouver</strong><br>
			Head Office<br>
			109 - 3551 Viking Way, Richmond, BC, V6V 1W1<br>
			T (604) 276-2366<br>
			F (604) 276-0860<br>
			<a href="mailto:ideas@xibita.ca">ideas@xibita.ca</a></p>
			<p><strong>Victoria</strong><br>
			#108 - 1027 Pandora Avenue, Victoria, BC, V8V 3P6<br>
			T (250) 413-3124<br>
			F (604) 276-0860<br>
			<a href="mailto:ideas@xibita.ca">ideas@xibita.ca</a></p>
			<p><strong>Calgary</strong><br>
			#10  2015 32nd Ave NE, Calgary, AB, T2E 6Z3<br>
			T (403) 261-5980<br>
			F (403) 261-5998<br>
			<a href="mailto:ideas@xibita.ca">ideas@xibita.ca</a></p>
			<p><strong>Toronto</strong><br>
			710 Gordon Baker Road, Toronto, ON, M2H 3B4<br>
			T (416) 494-9553<br>
			F (416) 494-7462<br>
			<a href="mailto:ideas@xibita.ca">ideas@xibita.ca</a></p>
			<p><strong>Cambridge</strong><br>
			6D 25 Struck Court, Cambridge, ON, N1R 8L2<br>
			T (519) 623-2989<br>
			F (519) 623-1714<br>
			<a href="mailto:ideas@xibita.ca">ideas@xibita.ca</a></p>
			<p><strong>Ottawa</strong><br>
			#9 190 Colonnade Road S., Ottawa, ON, K2E 7J5<br>
			T (613) 723-1849<br>
			F (613) 723-5489<br>
			<a href="mailto:ideas@xibita.ca">ideas@xibita.ca</a></p>
			<p><strong>Montreal</strong><br>
			Suite 309 1850 Boul. Le Corbusier, Laval, QC,  H7S 2K1<br>
			T (613) 723-1849<br>
			F (613) 723-5489<br>
			<a href="mailto:ideas@xibita.ca">ideas@xibita.ca</a></p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').orbit({
					timer:false
				});
		});
	</script>
  
</body>
</html>
