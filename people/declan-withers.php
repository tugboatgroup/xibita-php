<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Tradeshow exhibits, exhibit display, tradeshow services | xibita | Declan Withers</title>
	<meta name="description" content="Xibita has a leadership team that offers insight, expertise and assistance for your next tradeshow event.">
	<meta name="keywords" content="trade show exhibits, display booths or trade show design, trade show display, trade show exhibits, banner stands, trade show designs, display booth, digital printing, booth display, booth displays">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "about"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="slider">
			<img src="/images/banner-team.jpg" />
			</div>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Xibita's Leadership Team</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/about">About Us</a></li>
				<li class="forceborder"><a href="/team">People</a>
					<ul>
					  <li><a href="/people/doug-sjoberg">Doug Sjoberg</a></li>
						<li><a href="/people/ken-button">Ken Button</a></li>
						<li><a href="/people/karen-hansler">Karen Hansler</a></li>
						<li><a href="/people/dawnell-schumacher">Dawnell Schumacher</a></li>
					  <li><a href="/people/jason-morin">Jason Morin</a></li>
						<li class="active"><a href="/people/declan-withers">Declan Withers</a></li>
						<li><a href="/people/james-ayotte">James Ayotte</a></li>
						<li><a href="/people/aubin-symons">Aubin Symons</a></li>
					</ul>
				</li>
				<li><a href="/brochures">Corporate Brochures</a></li>
			</ul>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<h2><strong>Declan Withers</strong> <span>Exhibit Services Manager</span></h2>
			<p class="subheading">Creating a stress- and worry-free experience is an essential component of Xibita&rsquo;s exceptional customer service.</p>
			<p>Declan Withers has managed Xibita’s Exhibit Services Division since 2005. Declan is responsible for customer care, maintaining impeccable communication with the client through all aspects of the project. His expertise ranges from managing small local GTA installs to working with $1,000,000 exhibits as they find their way to international destinations. Declan’s attention to detail, his composure when the unexpected hits and his ability to find solutions under all conditions make him a unique asset to Xibita and their clients.</p>
			<p>Over the years, clients have come to rely on Declan to ensure that exhibit details are not overlooked. Declan works with Xibita’s clients to cost-effectively ship and install exhibits sometimes in very difficult situations. Working with Union Labor and balancing the requirements of clients demands a steady and calm demeanor. Declan has proven time after time his genius in managing and solving situations on the show floor.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').orbit({
					timer:false
				});
		});
	</script>
  
</body>
</html>
