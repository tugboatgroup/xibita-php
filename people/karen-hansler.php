<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Tradeshow exhibits, exhibit display, tradeshow services | xibita | Karen Hansler</title>
	<meta name="description" content="Xibita has a leadership team that offers insight, expertise and assistance for your next tradeshow event.">
	<meta name="keywords" content="trade show exhibits, display booths or trade show design, trade show display, trade show exhibits, banner stands, trade show designs, display booth, digital printing, booth display, booth displays">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "about"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="slider">
			<img src="/images/banner-team.jpg" />
			</div>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Xibita's Leadership Team</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/about">About Us</a></li>
				<li class="forceborder"><a href="/team">People</a>
					<ul>
					  <li><a href="/people/doug-sjoberg">Doug Sjoberg</a></li>
						<li><a href="/people/ken-button">Ken Button</a></li>
						<li class="active"><a href="/people/karen-hansler">Karen Hansler</a></li>
						<li><a href="/people/dawnell-schumacher">Dawnell Schumacher</a></li>
					  <li><a href="/people/jason-morin">Jason Morin</a></li>
						<li><a href="/people/declan-withers">Declan Withers</a></li>
						<li><a href="/people/james-ayotte">James Ayotte</a></li>
						<li><a href="/people/aubin-symons">Aubin Symons</a></li>
					</ul>
				</li>
				<li><a href="/brochures">Corporate Brochures</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<h2><strong>Karen Hansler</strong> <span>Director of Creative Services</span></h2>
			<p class="subheading">At Xibita, creativity is based on connecting the right ideas to solve exhibit and display challenges.</p>
			<p>Karen Hansler is Xibita’s Director of Creative Services. She graduated from The Ontario College of Art with a degree in Fine Art and Commercial Design. After graduation, Karen gained an extensive Graphic Design background at Roboshop, working with such clients as Remax, TMN, IBM, Safeway Foods, Jack Astors, Arctic Cat and Novartis.</p>
			<p>Karen brought her graphic design expertise to Xibita in 1996 to pursue a career in exhibit design. Over the past 21 years, Karen has become an expert in focusing client’s messages onto a wide range of portable, modular and custom exhibit environments. Karen consistently combines strong visuals with unique architecture to create exhibit solutions that exceed client expectations.</p>
			<p>Karen has worked with such brands as Rogers, ScotiaBank, Kellogs, McDonalds, The Source, University of Ottawa, CPA and Humber College. Karen has a countless array of Xibita clients that return to her year after year to enhance their exhibit through reconfigurations, additions, structural upgrades, graphic message changes and complete exhibit overhauls. She welcomes the challenges that come from working with her clients to achieve their goals in any capacity that is required.</p>

		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').orbit({
					timer:false
				});
		});
	</script>
  
</body>
</html>
