<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Fabric trade booths, red carpet backdrop, photography backdrop | xibita | Media Wall</title>
	<meta name="description" content="If you are looking for a portable stretch tension fabric displays for your next red carpet event &ndash; Xibita offers media walls that are lightweight and flexible.">
	<meta name="keywords" content="trade booths, trade show booths, tension fabric trade show booth, fabric show booths, displays trade show, step and repeat, red carpet backdrop, photography backdrop">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li><img src="/images/portables/slide-mediawall-01.jpg" alt="Tradewind & Media Wall Systems" title="Tradewind & Media Wall Systems"></li>
				</ul>
			</div>
			<?php include("includes/slides-portables.php"); ?>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Xibita&rsquo;s TRADE WIND fabric exhibit system and media walls offer flexibility and versatility.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a>
					<ul>
						<li><a href="/fabric/kado">Kado</a></li>
						<li><a href="/fabric/yello">Yello</a></li>
						<li><a href="/fabric/exhale">Exhale</a></li>
						<li class="active"><a href="/fabric/tradewind-media-wall">Trade Wind + Media Wall</a></li>
					</ul>
				</li>
				<li><a href="/modular-systems">Modular Systems</a></li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">Want to share your message from EVERY angle?  The TRADE WIND display system and a Media Wall create high impact.</p>
			<p>What lasting impression do you want to create?  As a personalized backdrop for all of your photos, a Media Wall from Xibita creates a lasting imprint of an event and promotes your brand wherever images are published.   Easy to assemble, the Media Wall is simply clicked into place, allowing you to set up the display easily and efficiently.</p>
			<p>From red carpet events to trade shows, the Media Wall provides a picture-perfect backdrop for all of your photographs.</p>
			<p><strong>TRADE WIND</strong> is the ultimate Point of Sale Display. With a limitless array of configurations, you  can have a different look every time you hit the show floor. TRADE WIND&rsquo;S changeable fabric graphics go on without any tools and remain on the display for easy set up and takedown.</p>
			<p>TRADE WIND allows you to create a 3-D environment with front and back graphics to create a greater visual impact. Add shelves and other accessories to highlight your products and your services.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21
			});

			$('#portfolio').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				startAt: 0,
				slideshow: false
			});
		});
	</script>
  
</body>
</html>
