<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Fabric trade show exhibits, fabric tradeshow exhibits, fabric tradeshow displays | xibita | Yello</title>
	<meta name="description" content="Portable displays that use fabric are easy to carry and easy to set up. Xibita can create a custom portable solution that is right for you.">
	<meta name="keywords" content="portable fabric tradeshow display, portable fabric tradeshow displays, fabric trade show banners, fabric trade show banner">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li><img src="/images/portables/slide-yello-01.jpg" alt="Yello Fabric System" title="Yello Fabric System"></li>
				</ul>
			</div>
			<?php include("includes/slides-portables.php"); ?>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>The YELLO modular display can address your tradeshow display needs.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a>
					<ul>
						<li><a href="/fabric/kado">Kado</a></li>
						<li class="active"><a href="/fabric/yello">Yello</a></li>
						<li><a href="/fabric/exhale">Exhale</a></li>
						<li><a href="/fabric/tradewind-media-wall">Trade Wind + Media Wall</a></li>
					</ul>
				</li>
				<li><a href="/modular-systems">Modular Systems</a></li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">With a one-year frame warranty, you can trust the quality of the YELLO fabric tradeshow system.</p>
			<p>If you are looking for a simple system that addresses your tradeshow requirements, YELLO is the ideal solution.</p>
			<p>Built to withstand the rigors of your show schedule, the YELLO fabric tradeshow system is constructed around a graphic back wall or hanging sign. It&rsquo;s easy to set-up, has lightweight construction, and is available in a variety of sizes and configurations.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21
			});

			$('#portfolio').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				startAt: 0,
				slideshow: false
			});
		});
	</script>
  
</body>
</html>
