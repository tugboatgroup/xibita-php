<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Tension fabric displays, portable displays, tradeshow displays | xibita | Kado</title>
	<meta name="description" content="The portable Kado fabric display system offers the perfect balance of customization and affordability.  Xibita has a solution that is perfect for you.">
	<meta name="keywords" content="portable fabric display, best fabric tradeshow display, best tradeshow fabric exhibit, portable tension fabric display">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li><img src="/images/portables/slide-kado-01.jpg" alt="Kado Fabric System" title="Kado Fabric System"></li>
					<li><img src="/images/portables/slide-kado-02.jpg" alt="Kado Fabric System" title="Kado Fabric System"></li>
				</ul>
			</div>
			<?php include("includes/slides-portables.php"); ?>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>KADO fabric displays offer the best modular approach for your display.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a>
					<ul>
						<li class="active"><a href="/fabric/kado">Kado</a></li>
						<li><a href="/fabric/yello">Yello</a></li>
						<li><a href="/fabric/exhale">Exhale</a></li>
						<li><a href="/fabric/tradewind-media-wall">Trade Wind + Media Wall</a></li>
					</ul>
				</li>
				<li><a href="/modular-systems">Modular Systems</a></li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">Customizable and durable, the KADO product line can meet all of your exhibit and trade show display requirements.</p>
			<p>The KADO product line combines affordability and customization that tests the boundaries of fabric structures. Limitless solutions are built into KADO.</p>
			<p>Made from 1&frac14;&rdquo; to 2&rdquo;aluminum pipe, this durable tension fabric display system has the ability to grow alongside your exhibit requirements. Build towers, counters or kiosks - it&rsquo;s the perfect product to &ldquo;mix and match,&rdquo; so your initial investment is always protected.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21
			});

			$('#portfolio').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				startAt: 0,
				slideshow: false
			});
		});
	</script>
  
</body>
</html>
