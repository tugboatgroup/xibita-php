<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Display booth, display stands, booth display| xibita | Portable Displays</title>
	<meta name="description" content="Xibita creates display stands, booth displays and display booths that generate attention and interest at your event.">
	<meta name="keywords" content="display booth, display booths, display stands, display stand, best display stands, best display stand ideas, best display stands.">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<?php include("includes/slides-portables.php"); ?>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Attract better prospects. Drive interest with portable display products.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li class="active"><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a></li>
				<li><a href="/modular-systems">Modular Systems</a></li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">In today&rsquo;s fast-paced trade show environment, you have seconds to capture the attention and interest of an audience on the move.</p>
			<p>Whether displaying in a shopping center, exhibiting at a trade show, or presenting at a conference, portable display products and services can deliver high-impact experiences that enhance your visibility in a "face-to-face" setting.</p>
			<p>Their modular ability allows you to increase or decrease your display dimensions with ease. If you need a repair, regardless of your location, our lifetime guarantee and national presence means we are never more than a phone call away.</p>
			<p>As your business grows, so do your portable display requirements. Starting with a portable display allows you to expand your exhibit easily and cost-effectively.</p>
			<hr>
			<h2>The Xibita Difference</h2>
			<p>In a competitive marketplace, Xibita stands out for its attention to detail, experience and solutions that generate better results. From the quality of our printing and products, to our ability to take care of any problems that may occur, rest assured we support your business growth.</p>
			<p>Our solutions offer:</p>
			<ul>
				<li><strong>Portability</strong>
					<ul>
						<li>Easy set-up</li>
						<li>Quick dismantle</li>
						<li>Compactness</li>
						<li>Superior visual impact imaging</li>
						<li>Customizable graphics</li>
						<li>Lifetime guarantee</li>
					</ul>
				<li><strong>Interchangeable accessories</strong></li>
				<li><strong>Quality manufacturing</strong></li>
				<li><strong>Fast production</strong></li>
				<li><strong>Wide product selection</strong></li>
				<li><strong>National repair locations</strong></li>
			</ul>
			<p>Xibita&rsquo;s innovative, eye-catching displays will maximize your brand awareness at your next important event.</p>
			<p><a href="docs/Xibita-Warranty.pdf" target="_blank">Download our Warranty information</a>.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				directionNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21,
				asNavFor: '#portfolio'
			});

			$('#portfolio').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav:false,
				selector: ".slides > .slide",
				sync: "#portfolionav"
			});

			$('#prjnerdcorps').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjsepps').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjpetcurean').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjdanier').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});
		});
	</script>
  
</body>
</html>
