<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Outdoor flags, promotional tents, sidewalk signs | xibita | Outdoor Products & Accessories</title>
	<meta name="description" content="Xibita has the right outdoor display products to share your message in any sort of weather.  Flags, promotional tents, window signs and sidewalk signs are available to promote your products and services.">
	<meta name="keywords" content="">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<?php include("includes/slides-seasonal.php"); ?>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Stand out from the crowd at your next outdoor event. Xibita can show you how.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a></li>
				<li><a href="/modular-systems">Modular Systems</a></li>
				<li class="active"><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">When the weather heats up or you need to add a little extra to your display - we&rsquo;ll help you stand out.</p>
			<p>Outdoor sports and social events, festivals, concerts, parades and increased foot traffic require displays that can withstand the trials of varied weather. Xibita&rsquo;s line of outdoor display products are produced with quality in mind. While we all know that Mother Nature cannot be controlled or predicted, Xibita&rsquo;s product mix will ensure that you message shines through, no matter what adverse conditions you face.</p>
			<p>When it&rsquo;s time to take your message outside for golf tournaments, festivals or events, Xibita&rsquo;s outdoor displays get you noticed. From our outdoor flags to our pop-up promotional tents, our options in outdoor display products offer great ways to promote products, support brand awareness and attract new customers.</p>
			<h2>Seasonal Products</h2>
			<div class="show-hide">
				<div class="accordionwrap">
					<h2 class="open">Feather Flag</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>A stunning shape and clear functionality a must for any sidewalk. This lightweight, portable and easy to assemble banner will showcase your message with single or dual-sided graphics.</p>
							</div>
							<div class="four columns">
								<img src="images/products/feather.jpg" width="232" height="309" alt="Feather flags" title="Feather flags">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/Feather-Flag-PRODSUM.pdf">Product Summary</a> (PDF)</li>
								<li><a href="/docs/ALLFLAGS-SETUP-GUIDE.pdf">Set-up Guide</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="accordionwrap">
					<h2>Teardrop Flag</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>A stunning shape and clear functionality a must for any side walk. This lightweight, portable and easy to assemble banner will showcase your message with single or dual-sided graphics.</p>
							</div>
							<div class="four columns">
								<img src="images/products/teardrop.jpg" width="232" height="309" alt="Teardrop flags" title="Teardrop flags">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/Teardrop-Flag-PRODSUM.pdf">Product Summary</a> (PDF)</li>
								<li><a href="/docs/ALLFLAGS-SETUP-GUIDE.pdf">Set-up Guide</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="accordionwrap">
					<h2>Store-front Flag</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>A stunning shape and clear functionality a must for any side walk. This lightweight, portable and easy to assemble banner will showcase your message with single or dual-sided graphics.</p>
							</div>
							<div class="four columns">
								<img src="images/products/storefront.jpg" width="232" height="309" alt="Storefront flags" title="Storefront flags">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/Storefront-Flag-PRODSUM.pdf">Product Summary</a> (PDF)</li>
								<li><a href="/docs/ALLFLAGS-SETUP-GUIDE.pdf">Set-up Guide</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="accordionwrap">
					<h2>Tent</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>Versatile, portable, practical tent is a highly visible sign of any event. This versatile and portable tent is perfect for any corporate event or celebratory occasion. With customizable graphics and frame thicknesses, this tent is the perfect solution for your event needs.</p>
							</div>
							<div class="four columns">
								<img src="images/products/tents.jpg" width="494" height="309" alt="Tents" title="Tents">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/Tent-PRODSUM.pdf">Product Summary</a> (PDF)</li>
								<li><a href="/docs/Tent-SETUP-GUIDE.pdf">Set-up Guide</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="accordionwrap">
					<h2>Trapeze</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>Simple, robust yet eye-catching TRAPEZE is ready to update whenever you are. This innovative sign literally brings eye-catching movement to your message with direct sidewalk exposure. Its unique design allows you to interchange your graphics with two top clips and easily update your graphics in an instant!</p>
							</div>
							<div class="four columns">
								<img src="images/products/trapeze.jpg" width="232" height="309" alt="Trapeze sidewalk signage" title="Trapeze sidewalk signage">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/Trapeze-PRODSUM.pdf">Product Summary</a> (PDF)</li>
								<li><a href="/docs/Trapeze-SETUP-GUIDE.pdf">Set-up Guide</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="accordionwrap">
					<h2>Venta</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>Face the front or rotate at any angle VENTA will show your best face to the world. Your windows have never seen anything like it. The easy-to-install interchangeable graphic will expose your message with simple suction-cup simplicity. VENTA can be rotated out onto the sidewalk rather than lying flat making your exposure ever-changing and eye-catching.</p>
							</div>
							<div class="four columns">
								<img src="images/products/venta.jpg" width="232" height="309" alt="Venta window signage" title="Venta window signage">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/Venta-PRODSUM.pdf">Product Summary</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<br>
			<br>
			<h2>Accessories</h2>
			<div class="show-hide">
				<div class="accordionwrap">
					<h2>Table Covers</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>Table covers offer a functional and customized look to any table with options to print on 3 or 4 sides. Branded table covers are the perfect finishing touch to fully customize your message.</p>
							</div>
							<div class="four columns">
								<img src="images/products/table-covers.jpg" width="494" height="309" alt="Table covers" title="Table covers">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/TableCover-SPECS.pdf">Specifications</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="accordionwrap">
					<h2>I-Lite</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>More information coming soon.</p>
							</div>
							<div class="four columns">
								<img src="images/products/ilite.jpg" width="494" height="309" alt="I-Lite" title="I-Lite">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/iLite-SPECS.pdf">Specifications</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="accordionwrap">
					<h2>Wall Washer Light</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>More information coming soon.</p>
							</div>
							<div class="four columns">
								<img src="images/products/wallwasher.jpg" width="494" height="309" alt="Wall Washer Light" title="Wall Washer Light">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/WallWasher-SPECS.pdf">Specifications</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="accordionwrap">
					<h2>Yello Clamp Light</h2>
					<div class="content">
						<div class="row">
							<div class="six columns">
								<p>An energy saving bright idea available with the YELLO product line. This energy-saving yet extremely bright 18 W LED clamp light is the perfect complement to showcase your feature wall.</p>
							</div>
							<div class="four columns">
								<img src="images/products/yello-clamplight.jpg" width="494" height="309" alt="Yello Clamp Light" title="Yello Clamp Light">
							</div>
						</div>
						<div class="downloads cf">
							<ul>
								<li><a href="/docs/Yello-Clamp-Light-SPECS.pdf">Specifications</a> (PDF)</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				directionNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21,
				asNavFor: '#portfolio'
			});

			$('#portfolio').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav:false,
				selector: ".slides > .slide",
				sync: "#portfolionav"
			});

			$('#prjaccessories1').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjaccessories2').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjaccessories3').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjaccessories4').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});
		});
	
	  new jQueryCollapse($(".show-hide"), {
		 query: 'div h2',
		 open: function() {
			this.slideDown(150);
		 },
		 close: function() {
			this.slideUp(150);
		 }
	  });
	</script>
  
</body>
</html>
