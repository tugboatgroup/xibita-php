<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Tradeshow exhibits, exhibit display, tradeshow services | xibita | Exhibit design services</title>
	<meta name="description" content="Designing and developing the right tradeshow exhibit display creates the right experience at your event.  Xibita offers tradeshow services to connect with your audience.">
	<meta name="keywords" content="trade show services, trade show booth design, tradeshow services, tradeshow booth, exhibit design company, tradeshow booths, tradeshow exhibit booth, tradeshow display booth, rental tradeshow exhibit.">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "services"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="slider">
			<img src="/images/banner-services.jpg" alt="banner photo" title="Tradeshow exhibits and exhibit displays  - Outstanding Show Services" />
			</div>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Tradeshow exhibits and exhibit displays - outstanding show services.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div id="maincopy" class="five columns offset-by-two small-8">
			<p class="subheading">Creating an Exhibit Marketing program that achieves a successful return on investment requires a high level of expertise. From event details to physical logistics, you can leverage our knowledge of coordinating and sharing best practices across your business units.</p>
			<p>With years of knowledge and hands-on experience, our experts assist you in overcoming any challenges in planning and producing the perfect tradeshow displays. We create flawless tradeshow exhibit presentations and experiences.</p>
			<p>As the only exhibit company in Canada with in-house expertise of two Certified Managers of Exhibits (CME's), our team members demonstrate a comprehensive understanding of the industry and a commitment that can address all aspects of your company&rsquo;s Exhibit Marketing Plan.</p>
			<p>Whether it&rsquo;s training and education or managing delivery and event-day logistics, our comprehensive range of Exhibit Marketing services will assist you in developing and implementing programs that increase your ROI and stack the odds of success in your favour.</p>
			<p>At Xibita, we believe in being held accountable for all products we offer. If you are not satisfied, please let us know&hellip;we&rsquo;ll make it right.</p>
			<ul class="accordion">
				<li>
					<div class="title">
						<h2>Installation + Dismantle</h2>
					</div>
					<div class="content">
						<p>When it comes to installation, Xibita can have your tradeshow exhibit set up and ready to go &ndash; so you can concentrate on having successful event. Similarly, after the event, we can dismantle your display so you can manage your time and maximize the connections you&rsquo;ve made. We will install, dismantle, ship and store your display to reduce the hassle associated with trade show participation and save you time. This method is much more affordable than you might think and offers great peace of mind.</p>
					</div>
				</li>
				<li>
					<div class="title">
						<h2>Storage</h2>
					</div>
					<div class="content">
						<p>If crates, tubs and display storage cases are overwhelming your office space, let Xibita take care of storing your exhibit in one of our climate-controlled facilities. You will always know that your exhibit will be show ready and available at a moment&rsquo;s notice.</p>
					</div>
				</li>
				<li>
					<div class="title">
						<h2>Exhibit Management</h2>
					</div>
					<div class="content">
						<p>When you need a partner to help you manage your exhibit program due to lack of internal resources, Xibita has the expertise to support you. From our online support tool through to our facilities across Canada and accessibility to our worldwide support team, no challenge is too great.</p>
					</div>
				</li>
				<li>
					<div class="title">
						<h2>Video Production</h2>
					</div>
					<div class="content">
						<p>Your exhibit is filled with monitors and you need the right inspiration to share your message. Xibita's in-house video production team can help you create the right visuals that are in harmony with your physical space.</p>
					</div>
				</li>
			</ul>
			<p>If you don't know how to take advantage of trade show exhibiting or environmental design, you may simply require additional knowledge. We focus on practical solutions that are time and resource efficient. Our goal is to support you and provide innovative solutions that result in your success.</p>
		</div>
		<div class="two small-8 columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').orbit({
					timer:false
				});
		});
	</script>
  
</body>
</html>
