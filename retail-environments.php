<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Retail environments, corporate displays, corporate environments | xibita | Retail and Corporate Environments</title>
	<meta name="description" content="If you are looking for a retail and corporate environment that stands out, Xibita can help.">
	<meta name="keywords" content="retail environment, retail environments, corporate environments, tradeshow exhibits, tradeshow displays, retail exhibit, retail exhibits, tradeshow exhibit, tradeshow display, corporate displays">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "exhibits"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<?php include("includes/slides-retail.php"); ?>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Retail and corporate environments redefined.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/exhibits">Custom Exhibits + Environments</a></li>
				<li class="active"><a href="/retail-environments">Retail +<br>Corporate Environments</a></li>
				<li><a href="/custom-graphics">Custom Graphics</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">When customers enter your store, or clients visit your business, subconsciously they judge and react to how you present products and services within your environment.</p>
			<p>Xibita can help create the right experience &ndash; whether you need an entire retail vision or minor assistance breathing new life into your corporate workspace.</p>
			<p>Our strength is based on connecting the disciplines of design, structural manufacturing, graphic production, and installation services under one roof.</p>
			<p>Whatever you desire, Xibita collaborates with you to bring a multi-disciplinary approach to your needs, and executes to deliver outstanding results.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				directionNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21,
				asNavFor: '#portfolio'
			});

			$('#portfolio').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav:false,
				selector: ".slides > .slide",
				sync: "#portfolionav"
			});

			$('#prjkelloggs').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjgoldrecyclers').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjkore').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prjdreamstar').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});
		});
	</script>
  
</body>
</html>
