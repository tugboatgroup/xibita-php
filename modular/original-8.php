<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Table top trade show display, tradeshow booth design, trade show stands | Xibita | Original 8 Display Booth</title>
	<meta name="description" content="Xibita designs and develops custom table top trade show displays, and trade show stands.">
	<meta name="keywords" content="Table top trade show display, tradeshow booth design, trade show stands, show booth displays, portable booth">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li><img src="/images/portables/slide-original8-01.jpg" alt="Original 8 Modular Systems" title="Original 8 Modular Systems"></li>
					<li><img src="/images/portables/slide-original8-02.jpg" alt="Original 8 Modular Systems" title="Original 8 Modular Systems"></li>
					<li><img src="/images/portables/slide-original8-03.jpg" alt="Original 8 Modular Systems" title="Original 8 Modular Systems"></li>
				</ul>
			</div>
			<?php include("includes/slides-portables.php"); ?>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>ORIGINAL 8 displays makes it easy to take the show on the road.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a></li>
				<li><a href="/modular-systems">Modular Systems</a>
					<ul>
						<li><a href="/modular/algo">Algo</a></li>
						<li><a href="/modular/network">Network</a></li>
						<li class="active"><a href="/modular/original-8">Original 8</a></li>
						<li><a href="/modular/satellite">Satellite</a></li>
					</ul>
				</li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">Durable and expandable, Original 8 can change as your needs change.</p>
			<p>ORIGINAL 8 is made of durable anodized aircraft aluminum. Its strong rectangular frames make the system almost impossible to damage with normal use. And with its patented 360 degree hinges and auto-locking device, you can expand, reduce or reconfigure ORIGINAL 8 as you wish.</p>
			<p>Each ORIGINAL 8 frame features twin channels that support both a front and back graphic panel for double the impact. Replacing graphic panels for a new look or promotion is easy and effortless with simple ball bearing release openings.</p>
			<p>The standard 10-foot exhibit compacts into a nylon carrying case that tucks neatly in the trunk of most cars. Once at the show just unpack and unfold&hellip;and display a full-size exhibit in seconds!</p>
			<h2>Table Top Trade Show Display Series</h2>
			<p>If space is of a premium for you, the Original 8 table top series provides a modular smaller-scale solution that can also be expanded into a full-size floor standing exhibit should your needs change.</p>
			<p>Each system offers an attractive backdrop and supports any of our three panels:</p>
			<ul>
				<li>Fabric</li>
				<li>Designer finish</li>
				<li>Printed graphic</li>
			</ul>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21
			});

			$('#portfolio').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				startAt: 0,
				slideshow: false
			});
		});
	</script>
  
</body>
</html>
