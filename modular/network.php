<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Portable trade show displays, table top portable display booths | xibita | Network Display Booth</title>
	<meta name="description" content="Xibita designs and develops custom portable trade show displays, exhibits, and booths.">
	<meta name="keywords" content="portable display booths, portable display booth, display trade show, trade show displays">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li><img src="/images/portables/slide-network-01.jpg" alt="Network Modular Systems" title="Network Modular Systems"></li>
					<li><img src="/images/portables/slide-network-02.jpg" alt="Network Modular Systems" title="Network Modular Systems"></li>
					<li><img src="/images/portables/slide-network-03.jpg" alt="Network Modular Systems" title="Network Modular Systems"></li>
				</ul>
			</div>
			<?php include("includes/slides-portables.php"); ?>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Image is everything&hellip; especially at your next event or trade show.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a></li>
				<li><a href="/modular-systems">Modular Systems</a>
					<ul>
						<li><a href="/modular/algo">Algo</a></li>
						<li class="active"><a href="/modular/network">Network</a></li>
						<li><a href="/modular/original-8">Original 8</a></li>
						<li><a href="/modular/satellite">Satellite</a></li>
					</ul>
				</li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">With smooth curves and seamless presentation, the NETWORK display line is hard to miss.</p>
			<p>Made of durable anodized aircraft aluminum, this Network modular display system offers rugged good looks and is virtually impossible to break with normal use. Its lightweight strength and rigidity is unmatched in the industry.</p>
			<p>The Network display frame collapses neatly into a single portable All-In-One Case and transforms into an attractive Podium to create a dramatic presence and add impact to your overall presentation.</p>
			<p>Designed to be expanded, reduced or reconfigured without tools or tinkering. Change your trade show display from a flat wall to a curve or an S-curve. Or, build it into a unique arch that towers above.  Network is portability combined with the flexibility you need.</p>
			<h2>Table Top Trade Show Display Series</h2>
			<p>If space is of a premium for you, the Network table top series provides a modular solution that can be expanded into a full-size floor standing exhibit as your business grows.</p>
			<p>Each system offers an attractive backdrop and supports any of our three panels:</p>
			<ul>
				<li>Fabric</li>
				<li>Designer finish</li>
				<li>Printed graphic</li>
			</ul>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21
			});

			$('#portfolio').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				startAt: 0,
				slideshow: false
			});
		});
	</script>
  
</body>
</html>
