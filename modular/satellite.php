<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Tradeshow display system, portable exhibit display, tension fabric displays| xibita | Satellite Display System</title>
	<meta name="description" content="Xibita&rsquo;s Satellite display system is light weight, easy to set up, and looks great from any angle.">
	<meta name="keywords" content="trade show displays, trade show booths, portable display booth, trade show exhibit, pop up displays, tension fabric display booth">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "portables"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li><img src="/images/portables/slide-satellite-01.jpg" alt="Satellite Systems" title="Satellite Modular Systems"></li>
				</ul>
			</div>
			<?php include("includes/slides-portables.php"); ?>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>The SATELLITE display system offers high impact.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/portable-displays">The Portables<br>Display Systems</a></li>
				<li><a href="/banner-stands">Banner Stands</a></li>
				<li><a href="/fabric-systems">Fabric Systems</a></li>
				<li><a href="/modular-systems">Modular Systems</a>
					<ul>
						<li><a href="/modular/algo">Algo</a></li>
						<li><a href="/modular/network">Network</a></li>
						<li><a href="/modular/original-8">Original 8</a></li>
						<li class="active"><a href="/modular/satellite">Satellite</a></li>
					</ul>
				</li>
				<li><a href="/seasonal-products">Seasonal Products + Accessories</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">Command attention in any room; communicate from any angle.</p>
			<p>From top to bottom, side to side, the SATELLITE&rsquo;s strong and sturdy presence commands attention, while its elegant appearance invites admiration. It handles itself well in any room and is ready to make a statement at a moment's notice.</p>
			<p>Its removable seamless graphic panels can be dressed to fit each occasion and the structure itself can be used as a key architectural component in large-scale exhibits.</p>
			<p>The SATELLITE is easy to set up in minutes and take down just as fast. Expand the lightweight frames, lock them into place and connect the panels. When you're done, take it down and pack it away in the Nylon Carrying Case - another example of power through simplicity.  Available in a 3-section tower or a 4-section high-rise to meet your specific communication needs.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21
			});

			$('#portfolio').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				startAt: 0,
				slideshow: false
			});
		});
	</script>
  
</body>
</html>
