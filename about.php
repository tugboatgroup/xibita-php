<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Tradeshow exhibits, exhibit display, tradeshow services | xibita | About</title>
	<meta name="description" content="If you need help with your trade show exhibits, display booths or trade show design, Xibitia can help.">
	<meta name="keywords" content="trade show exhibits, display booths or trade show design, trade show display, trade show exhibits, banner stands, trade show designs, display booth, digital printing, booth display, booth displays">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "about"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="slider">
			<img src="/images/banner-about.jpg" alt="banner photo" title="Your Vision, Our Trade Show Design Expertise" />
			</div>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Your vision, our trade show design expertise.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li class="active"><a href="/about">About Us</a></li>
				<li><a href="/team">People</a></li>
				<li><a href="/brochures">Corporate Brochures</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">We are a team of well-organized, capable and adaptable exhibit designers, display developers, project and event managers. We are passionate about creating extraordinary experiences and environments.</p>
			<p>Xibita has over 50 years&rsquo; experience in custom award winning exhibit designs and environments on a national scale.</p>
			<p>Whether its portable banner stands or advanced custom exhibit displays, we take pride in the products and services that our people and solutions offer.</p>
			<h2>Our Commitment to You</h2>
			<p>We are dedicated to:</p>
			<ul>
				<li>Consulting - to fully understand your exhibit design needs</li>
				<li>Clarifying - to refine your core messages and brand requirements</li>
				<li>Knowledge - to offering updates and insights regarding production and products</li>
				<li>Timeliness - to delivering projects on time</li>
				<li>Solutions - to assisting our clients to achieve success</li>
			</ul>
			<p>Let your message stand out from your competition.  With Xibita&rsquo;s insight and expertise, you can concentrate on connecting and engaging with your clients and prospects.</p>
			<p>If you have questions about how to reduce time and effort regarding your promotions at seminars, trade shows or events, or how you can connect and collect information from your prospects - <a href="contact-us.php">let us help</a>.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').orbit({
					timer:false
				});
		});
	</script>
  
</body>
</html>
