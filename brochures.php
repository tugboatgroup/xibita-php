<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Tradeshow exhibits, exhibit display, tradeshow services | xibita | Brochures</title>
	<meta name="description" content="If you need help with your trade show exhibits, display booths or trade show design, Xibitia can help.">
	<meta name="keywords" content="trade show exhibits, display booths or trade show design, trade show display, trade show exhibits, banner stands, trade show designs, display booth, digital printing, booth display, booth displays">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>

	<?php $section = "about"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<div class="row">
		<div class="eight columns centered">
			<div id="slider">
			<img src="/images/banner-about.jpg" alt="banner photo" title="Your Vision, Our Trade Show Design Expertise" />
			</div>
		</div>
	</div>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Corporate Brochures</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li><a href="/about">About Us</a></li>
				<li><a href="/team">People</a></li>
				<li class="active"><a href="/brochures">Corporate Brochures</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
	</div>
	<div class="row">
		<div id="maincopy" class="eight columns centered">

			<div id="brochure-thumbs">
				<ul>
					<li>
						<a href="/docs/Xibita-OVERVIEW-Brochure.pdf" target="_blank">
							<img src="/images/brochures/Xibita-OVERVIEW-Brochure-thumb.jpg">
						</a>
						<a href="/docs/Xibita-OVERVIEW-Brochure.pdf" target="_blank" class="download">Download PDF</a>
						<span>Overview</span>
					</li>
					<li>
						<a href="/docs/Xibita-EXHIBITS-Brochure.pdf" target="_blank">
							<img src="/images/brochures/Xibita-EXHIBITS-Brochure-thumb.jpg">
						</a>
						<a href="/docs/Xibita-OVERVIEW-Brochure.pdf" target="_blank" class="download">Download PDF</a>
						<span>Exhibits</span>
					</li>
					<li>
						<a href="/docs/Xibita-ENVIRONMENTS-Brochure.pdf" target="_blank">
							<img src="/images/brochures/Xibita-ENVIRONMENTS-Brochure-thumb.jpg">
						</a>
						<a href="/docs/Xibita-OVERVIEW-Brochure.pdf" target="_blank" class="download">Download PDF</a>
						<span>Environments</span>
					</li>
					<li>
						<a href="/docs/Xibita-GRAPHICS-Brochure.pdf" target="_blank">
							<img src="/images/brochures/Xibita-GRAPHICS-Brochure-thumb.jpg">
						</a>
						<a href="/docs/Xibita-OVERVIEW-Brochure.pdf" target="_blank" class="download">Download PDF</a>
						<span>Graphics</span>
					</li>
				</ul>
			</div>

		</div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
  	<script src="/javascripts/vendor/jquery.scrollTo.min.js"></script>
	<script src="/javascripts/vendor/pdf.js"></script>
	<script src="/javascripts/pdf-viewer.js"></script>

	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
				$('#slider').orbit({
					timer:false
				});
		});
	</script>
  
</body>
</html>
