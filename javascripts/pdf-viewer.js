var viewer = {
	currPdfPage: 0,
	maxPdfPages: 0,
	pdfSrc: null,
	init: function(){

		var screenProps = $(window)[0].screen;

		var isMobile = false;
		if($(window)[0].orientation == 90 || $(window)[0].orientation == -90){
			
			// Landscape
			if(screenProps.availWidth <= 812){
				// iPhone X and below
				isMobile = true;
			}

		}else{

			// Portrait
			if(screenProps.availWidth <= 414){
				// iPhone X and below
				isMobile = true;
			}

		}

		var $thumbLinks = $("#brochure-thumbs ul li a:not(.download)");
		if(!isMobile){

			// Setup PDF viewer
			viewer.setupViewer($thumbLinks);

		}
		
	},
	generateThumb: function($el){

		var PDFUrl = $el.prop('href');

		PDFJS.getDocument(PDFUrl).promise.then(function (doc){

		  var pages = []; 
		  while (pages.length < 1){
		  	pages.push(pages.length + 1);
		  }
		  
		  return Promise.all(pages.map(function (num) {

		    return doc.getPage(num).then(viewer.makeThumb)
		      .then(function (canvas) {
		        $el.append(canvas);
		    });

		  }));

		}).catch(console.error);

	},
	setupViewer: function($thumbLinks){

		var $canvas = $("canvas#pdf-catcher");
		if($canvas.length == 0){

			// Create catcher canvas element
			var catcher = $("<div id=\"pdf-container\" class=\"off\"><canvas id=\"pdf-catcher\"></canvas></div>")

			$("#brochure-thumbs").after(catcher);
			$canvas = $("canvas#pdf-catcher");

			// Create prev / next page controls
			var pageControls = $("<div class=\"pdf-pager off\"><a href=\"#\" class=\"prev-page\"></a><a href=\"#\" class=\"next-page\"></a><div class=\"page-count\"></div></div>");
			$("#pdf-container").append(pageControls);

		}

		// Bind click events
		$thumbLinks.off("click").on("click", function(ev){
			ev.preventDefault();

			$("#brochure-thumbs ul li").removeClass("selected");
			$(this).parent().addClass("selected");

			var srcPdf = $(this).prop('href');
			viewer.pdfSrc = srcPdf;
			PDFJS.workerSrc = '/javascripts/vendor/pdf.worker.js';
			
			viewer.renderPdfPage(srcPdf, 1);

		});

		var $pdfPagerLinks = $(".pdf-pager a");
		$pdfPagerLinks.off("click").on("click", function(ev){
			ev.preventDefault();

			var srcPdf = viewer.pdfSrc;
			var nextPage = null;

			switch($(this).prop("class")){
				case 'prev-page':

					nextPage = viewer.currPdfPage - 1;
					if(nextPage < 1){
						nextPage = null;
					}

				break;
				case 'next-page':
					nextPage = viewer.currPdfPage + 1;
					if(nextPage > viewer.maxPdfPages){
						nextPage = null;
					}

				break;
			}

			if(nextPage !== null && srcPdf !== null){

				viewer.renderPdfPage(srcPdf, nextPage);

			}

		});

		
	},
	renderPdfPage: function(srcPdf, pageNo){

		PDFJS.getDocument(srcPdf).then(function(pdf) {

		  viewer.maxPdfPages = pdf.pdfInfo.numPages;

		  pdf.getPage(pageNo).then(function(page) {
		    var scale = 1.5;
		    var viewport = page.getViewport(scale);

		    var canvas = document.getElementById('pdf-catcher');
		    var context = canvas.getContext('2d');
		    canvas.height = viewport.height;
		    canvas.width = viewport.width;

		    var renderContext = {
		      canvasContext: context,
		      viewport: viewport
		    };
		    page.render(renderContext);

		    $("#pdf-catcher").addClass("loaded");
		    $(".pdf-pager").removeClass("off");

		    viewer.currPdfPage = pageNo;

		    if(parseFloat(viewer.currPdfPage - 1) < 1){
			    $(".pdf-pager .prev-page").addClass("dis");
		    }else{
			    $(".pdf-pager .prev-page").removeClass("dis");
		    }

		    if(parseFloat(viewer.currPdfPage + 1) > viewer.maxPdfPages){
			    $(".pdf-pager .next-page").addClass("dis");
		    }else{
			    $(".pdf-pager .next-page").removeClass("dis");
		    }

		    $(".pdf-pager .page-count").text("Page " + viewer.currPdfPage + " of " + viewer.maxPdfPages);

			$(window).scrollTo(canvas, 800, {
				axis: 'y',
				offset: -200
			});

		  });

		});					

	},
	makeThumb(page) {
	  // draw thumb to fit into 150x150 canvas
	  var vp = page.getViewport(1);

	  var canvas = document.createElement("canvas");
	  canvas.width = 150;
	  canvas.height = 115;

	  var scale = Math.min(canvas.width / vp.width, canvas.height / vp.height);
	  return page.render({canvasContext: canvas.getContext("2d"), viewport: page.getViewport(scale)}).promise.then(function () {
	    return canvas;
	  });

	}
}

$(document).ready(function(){

	viewer.init();

});