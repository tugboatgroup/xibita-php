<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li class="slide">
						<div id="prjnerdcorps" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-portables-nerdcorps.jpg" alt="Portables slide" title="Nerd Corps Entertainment">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjsepps" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-portables-sepps.jpg" alt="Portables slide" title="Sepp's Gourmet Foods">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjpetcurean" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-portables-petcurean.jpg" alt="Portables slide" title="Petcurean Pet Nutrition">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjdanier" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-portables-danier.jpg" alt="Portables slide" title="Danier Leather">
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div id="portfolionav" class="flexslider">
				<ul class="slides">
					<li>
						<img src="/images/portables/slide-portables-nerdcorps-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-portables-sepps-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-portables-petcurean-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-portables-danier-t.jpg" alt="" title="">
					</li>
				</ul>
			</div>
		</div>
	</div>