<!-- Included CSS Files (Uncompressed) -->
	<!--
	<link rel="stylesheet" href="/stylesheets/foundation.css">
	-->
  
	<!-- Included CSS Files (Compressed) -->
	<link rel="stylesheet" href="/stylesheets/foundation.min.css">
	<link rel="stylesheet" href="/stylesheets/flexslider.css">
	<link rel="stylesheet" href="/stylesheets/xibita.css">

	<script src="/javascripts/modernizr.foundation.js"></script>
	<script src="/javascripts/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-instagram/0.2.2/jquery.instagram.min.js"></script>
	<script type="text/javascript">
		var _ss = _ss || [];
		_ss.push(['_setDomain', 'https://koi-3QN5BLULVK.marketingautomation.services/net']);
		_ss.push(['_setAccount', 'KOI-3RFAYJSND4']);
		_ss.push(['_trackPageView']);
		(function() {
			var ss = document.createElement('script');
			ss.type = 'text/javascript'; ss.async = true;

			ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QN5BLULVK.marketingautomation.services/client/ss.js?ver=1.1.1';
			var scr = document.getElementsByTagName('script')[0];
			scr.parentNode.insertBefore(ss, scr);
		})();
	</script>
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">