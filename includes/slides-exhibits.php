<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li class="slide">
						<div id="prj01" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-uottawa-01.jpg" alt="uOttawa project slide" title="uOttawa">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-uottawa-02.jpg" alt="uOttawa project slide" title="uOttawa">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-uottawa-03.jpg" alt="uOttawa project slide" title="uOttawa">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-uottawa-04.jpg" alt="uOttawa project slide" title="uOttawa">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj02" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-aperio-01.jpg" alt="Aperio project slide" title="Aperio">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-aperio-02.jpg" alt="Aperio project slide" title="Aperio">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-aperio-03.jpg" alt="Aperio project slide" title="Aperio">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj03" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-onetouch-01.jpg" alt="One Touch project slide" title="One Touch">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-onetouch-02.jpg" alt="One Touch project slide" title="One Touch">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-onetouch-03.jpg" alt="One Touch project slide" title="One Touch">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj04" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-daiya-01.jpg" alt="Daiya project slide" title="Daiya">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-daiya-02.jpg" alt="Daiya project slide" title="Daiya">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-daiya-03.jpg" alt="Daiya project slide" title="Daiya">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj05" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-carleton-01.jpg" alt="Carleton project slide" title="Carleton">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-carleton-02.jpg" alt="Carleton project slide" title="Carleton">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-carleton-03.jpg" alt="Carleton project slide" title="Carleton">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj06" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-behar-01.jpg" alt="Behar project slide" title="Behar">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-behar-02.jpg" alt="Behar project slide" title="Behar">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-behar-03.jpg" alt="Behar project slide" title="Behar">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj07" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-sheridan-01.jpg" alt="Sheridan project slide" title="Sheridan">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-sheridan-02.jpg" alt="Sheridan project slide" title="Sheridan">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-sheridan-03.jpg" alt="Sheridan project slide" title="Sheridan">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj08" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-sophos-01.jpg" alt="Sophos project slide" title="Sophos">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-sophos-02.jpg" alt="Sophos project slide" title="Sophos">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj09" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-algoma-01.jpg" alt="Algoma project slide" title="Algoma">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-algoma-02.jpg" alt="Algoma project slide" title="Algoma">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-algoma-03.jpg" alt="Algoma project slide" title="Algoma">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj10" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-reliabrand-01.jpg" alt="Reliabrand project slide" title="Reliabrand">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-reliabrand-02.jpg" alt="Reliabrand project slide" title="Reliabrand">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-reliabrand-03.jpg" alt="Reliabrand project slide" title="Reliabrand">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj11" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-queens-01.jpg" alt="Queens project slide" title="Queens">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-queens-02.jpg" alt="Queens project slide" title="Queens">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-queens-03.jpg" alt="Queens project slide" title="Queens">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj12" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-cga-01.jpg" alt="CGA project slide" title="CGA">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-cga-02.jpg" alt="CGA project slide" title="CGA">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-cga-03.jpg" alt="CGA project slide" title="CGA">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj13" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-trent-01.jpg" alt="Trent project slide" title="Trent">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-trent-02.jpg" alt="Trent project slide" title="Trent">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-trent-03.jpg" alt="Trent project slide" title="Trent">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj14" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-canadapost-01.jpg" alt="Canada Post project slide" title="Canada Post">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-canadapost-02.jpg" alt="Canada Post project slide" title="Canada Post">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-canadapost-03.jpg" alt="Canada Post project slide" title="Canada Post">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj15" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-uoit-01.jpg" alt="UOIT project slide" title="UOIT">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-uoit-02.jpg" alt="UOIT project slide" title="UOIT">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-uoit-03.jpg" alt="UOIT project slide" title="UOIT">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj16" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-bbc-01.jpg" alt="BBC Sports project slide" title="BBC Sports">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj17" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-monoserra-01.jpg" alt="Mono Serra project slide" title="Mono Serra">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-monoserra-02.jpg" alt="Mono Serra project slide" title="Mono Serra">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-monoserra-03.jpg" alt="Mono Serra project slide" title="Mono Serra">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj18" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-octapharma-01.jpg" alt="Octapharma project slide" title="Octapharma">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-octapharma-02.jpg" alt="Octapharma project slide" title="Octapharma">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj19" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-cakerie-01.jpg" alt="Cakerie project slide" title="Cakerie">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-cakerie-02.jpg" alt="Cakerie project slide" title="Cakerie">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-cakerie-03.jpg" alt="Cakerie project slide" title="Cakerie">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj20" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-citox-01.jpg" alt="CiTox project slide" title="CiTox">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-citox-02.jpg" alt="CiTox project slide" title="CiTox">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-citox-03.jpg" alt="CiTox project slide" title="CiTox">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj21" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-encana-01.jpg" alt="Encana project slide" title="Encana">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-encana-02.jpg" alt="Encana project slide" title="Encana">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-encana-03.jpg" alt="Encana project slide" title="Encana">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj22" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-esthetique-01.jpg" alt="Esthetique Spa project slide" title="Esthetique Spa">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-esthetique-02.jpg" alt="Esthetique Spa project slide" title="Esthetique Spa">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-esthetique-03.jpg" alt="Esthetique Spa project slide" title="Esthetique Spa">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj23" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-ics-01.jpg" alt="ICS project slide" title="ICS">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-ics-02.jpg" alt="ICS project slide" title="ICS">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-ics-03.jpg" alt="ICS project slide" title="ICS">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj24" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-pco-01.jpg" alt="PCO project slide" title="PCO">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-pco-02.jpg" alt="PCO project slide" title="PCO">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-pco-03.jpg" alt="PCO project slide" title="PCO">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj25" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-fortis-01.jpg" alt="Fortis project slide" title="Fortis">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-fortis-02.jpg" alt="Fortis project slide" title="Fortis">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-fortis-03.jpg" alt="Fortis project slide" title="Fortis">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prj26" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-puresource-01.jpg" alt="Pure Source project slide" title="Pure Source">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-puresource-02.jpg" alt="Pure Source project slide" title="Pure Source">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-puresource-03.jpg" alt="Pure Source project slide" title="Pure Source">
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div id="portfolionav" class="flexslider">
				<ul class="slides">
					<li>
						<img src="/images/exhibits/slide-uottawa-t.jpg" alt="uOttawa" title="uOttawa">
					</li>
					<li>
						<img src="/images/exhibits/slide-aperio-t.jpg" alt="Aperio" title="Aperio">
					</li>
					<li>
						<img src="/images/exhibits/slide-onetouch-t.jpg" alt="One Touch" title="One Touch">
					</li>
					<li>
						<img src="/images/exhibits/slide-daiya-t.jpg" alt="Daiya" title="Daiya">
					</li>
					<li>
						<img src="/images/exhibits/slide-carleton-t.jpg" alt="Carleton" title="Carleton">
					</li>
					<li>
						<img src="/images/exhibits/slide-behar-t.jpg" alt="Behar" title="Behar">
					</li>
					<li>
						<img src="/images/exhibits/slide-sheridan-t.jpg" alt="Sheridan" title="Sheridan">
					</li>
					<li>
						<img src="/images/exhibits/slide-sophos-t.jpg" alt="Sophos" title="Sophos">
					</li>
					<li>
						<img src="/images/exhibits/slide-algoma-t.jpg" alt="Algoma" title="Algoma">
					</li>
					<li>
						<img src="/images/exhibits/slide-reliabrand-t.jpg" alt="Reliabrand" title="Reliabrand">
					</li>
					<li>
						<img src="/images/exhibits/slide-queens-t.jpg" alt="Queens" title="Queens">
					</li>
					<li>
						<img src="/images/exhibits/slide-cga-t.jpg" alt="CGA" title="CGA">
					</li>
					<li>
						<img src="/images/exhibits/slide-trent-t.jpg" alt="Trent" title="Trent">
					</li>
					<li>
						<img src="/images/exhibits/slide-canadapost-t.jpg" alt="Canada Post" title="Canada Post">
					</li>
					<li>
						<img src="/images/exhibits/slide-uoit-t.jpg" alt="UOIT" title="UOIT">
					</li>
					<li>
						<img src="/images/exhibits/slide-bbc-t.jpg" alt="BBC Sports" title="BBC Sports">
					</li>
					<li>
						<img src="/images/exhibits/slide-monoserra-t.jpg" alt="Mono Serra" title="Mono Serra">
					</li>
					<li>
						<img src="/images/exhibits/slide-octapharma-t.jpg" alt="Octapharma" title="Octapharma">
					</li>
					<li>
						<img src="/images/exhibits/slide-cakerie-t.jpg" alt="Cakerie" title="Cakerie">
					</li>
					<li>
						<img src="/images/exhibits/slide-citox-t.jpg" alt="CiTox" title="CiTox">
					</li>
					<li>
						<img src="/images/exhibits/slide-encana-t.jpg" alt="Encana" title="Encana">
					</li>
					<li>
						<img src="/images/exhibits/slide-esthetique-t.jpg" alt="Esthetique Spa" title="Esthetique Spa">
					</li>
					<li>
						<img src="/images/exhibits/slide-ics-t.jpg" alt="ICS" title="ICS">
					</li>
					<li>
						<img src="/images/exhibits/slide-pco-t.jpg" alt="PCO" title="PCO">
					</li>
					<li>
						<img src="/images/exhibits/slide-fortis-t.jpg" alt="Fortis" title="Fortis">
					</li>
					<li>
						<img src="/images/exhibits/slide-puresource-t.jpg" alt="Pure Source" title="Pure Source">
					</li>
				</ul>
			</div>
		</div>
	</div>