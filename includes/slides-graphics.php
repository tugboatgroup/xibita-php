<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li class="slide">
						<div id="prjaw" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-aw-01.jpg" alt="A&W project slide" title="A&W">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-aw-02.jpg" alt="A&W project slide" title="A&W">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-aw-03.jpg" alt="A&W project slide" title="A&W">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjshaw" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-shaw-01.jpg" alt="Shaw project slide" title="Shaw">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjgeneralpaint" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-generalpaint-01.jpg" alt="General Paint project slide" title="General Paint">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjlulu" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-lulu-01.jpg" alt="Lululemon Athletica project slide" title="Lululemon Athletica">
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div id="portfolionav" class="flexslider">
				<ul class="slides">
					<li>
						<img src="/images/exhibits/slide-aw-t.jpg" alt="A&W" title="A&W">
					</li>
					<li>
						<img src="/images/exhibits/slide-shaw-t.jpg" alt="Shaw" title="Shaw">
					</li>
					<li>
						<img src="/images/exhibits/slide-generalpaint-t.jpg" alt="General Paint" title="General Paint">
					</li>
					<li>
						<img src="/images/exhibits/slide-lulu-t.jpg" alt="Lulu" title="Lululemon Athletica">
					</li>
				</ul>
			</div>
		</div>
	</div>