<div id="alt-subnav">
				<select onChange="window.location.replace(this.options[this.selectedIndex].value)">
					<option>Section Navigation</option>
					<option value="/about">ABOUT US</option>
					<option value="/team">PEOPLE</option>
					<option value="/people/ken-button"> - Ken Button</option>
					<option value="/people/karen-hansler"> - Karen Hansler</option>
					<option value="/people/jim-ferreira"> - Jim Ferreira</option>
					<option value="/people/declan-withers"> - Declan Withers</option>
					<option value="/people/paul-talbot"> - Paul Talbot</option>
					<option value="/people/james-ayotte"> - James Ayotte</option>
					<option value="/people/aubin-symons"> - Aubin Symons</option>
					<option value="/brochures">CORPORATE BROCHURES</option>
				</select>
			</div>