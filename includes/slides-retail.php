<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li class="slide">
						<div id="prjkelloggs" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-kelloggs-01.jpg" alt="Kelloggs project slide" title="Kelloggs">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-kelloggs-02.jpg" alt="Kelloggs project slide" title="Kelloggs">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-kelloggs-03.jpg" alt="Kelloggs project slide" title="Kelloggs">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-kelloggs-04.jpg" alt="Kelloggs project slide" title="Kelloggs">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjgoldrecyclers" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-goldrecyclers-01.jpg" alt="Gold Recyclers project slide" title="Gold Recyclers">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjkore" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-kore-01.jpg" alt="Kore project slide" title="Kore">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-kore-02.jpg" alt="Kore project slide" title="Kore">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-kore-03.jpg" alt="Kore project slide" title="Kore">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjdreamstar" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/exhibits/slide-dreamstar-01.jpg" alt="Dreamstar project slide" title="Dreamstar">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-dreamstar-02.jpg" alt="Dreamstar project slide" title="Dreamstar">
								</li>
								<li class="projectslide">
									<img src="/images/exhibits/slide-dreamstar-03.jpg" alt="Dreamstar project slide" title="Dreamstar">
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div id="portfolionav" class="flexslider">
				<ul class="slides">
					<li>
						<img src="/images/exhibits/slide-kelloggs-t.jpg" alt="Kelloggs" title="Kelloggs">
					</li>
					<li>
						<img src="/images/exhibits/slide-goldrecyclers-t.jpg" alt="Gold Recyclers" title="Gold Recyclers">
					</li>
					<li>
						<img src="/images/exhibits/slide-kore-t.jpg" alt="Kore" title="Kore">
					</li>
					<li>
						<img src="/images/exhibits/slide-dreamstar-t.jpg" alt="Dreamstar" title="Dreamstar">
					</li>
				</ul>
			</div>
		</div>
	</div>