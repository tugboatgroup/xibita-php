<header id="main-header">
		<div class="row">
			<div class="eight columns centered">
				<div class="row">
					<div class="ten columns">
						<nav id="utility">
							<ul>
								<li><a href="/about">About Us</a></li>
								<li><a href="/contact-us">Contact Us</a></li>
								<li class="btn-blog"><a href="/ftp">FTP</a></li>
								<!--li class="btn-blog"><a href="http://blog.xibita.com/">Blog</a></li-->
							</ul>
						</nav>
					</div>
				</div>
				<div id="row-logo-nav" class="row">
					<div id="logo"><a href="/"><img src="/images/xibita-logo.png" alt="Xibita logo" title="Xibita - Exhibit design, environments, trade show displays and graphics that generate results."></a></div>
					<nav id="primary">
						<select id="alt-nav" onChange="window.location.replace(this.options[this.selectedIndex].value)">
							<option>Navigation</option>
							<option value="/">Xibita Home</option>
							<option value="/exhibits">CUSTOM EXHIBITS + ENVIRONMENTS</option>
							<option value="/retail-environments"> - Retail and Corporate Environments</option>
							<option value="/custom-graphics"> - Custom Graphics</option>
							<option value="/portable-displays">THE PORTABLES DISPLAY SYSTEMS</option>
							<option value="/banner-stands"> - Roll-Up &amp; Retractable Banners</option>
							<option value="/fabric-systems"> - Fabric Systems</option>
							<option value="/modular-systems"> - Modular Systems</option>
							<option value="/outdoor-products"> - Outdoor Products &amp; Accessories</option>
							<option value="/exhibit-design-services">SERVICES</option>
							<option>MORE...</option>
							<option value="/about">ABOUT US</option>
							<option value="/team"> - Leadership Team</option>
							<option value="/contact-us">CONTACT US</option>
							<option value="/locations"> - Locations</option>
							<option value="/ftp">FTP</option>
							<!--option value="http://blog.xibita.com/">Blog</option-->
						</select>
						<ul class="tiny-hide">
							<li id="nav-custom"><a href="/exhibits">Custom Exhibits + Environments</a></li>
							<li id="nav-portables"><a href="/portable-displays">The Portables Display Systems</a></li>
							<li id="nav-services"><a href="/exhibit-design-services">Services</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>