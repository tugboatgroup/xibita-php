<div class="row">
		<div class="eight columns centered">
			<div id="portfolio" class="flexslider">
				<ul class="slides">
					<li class="slide">
						<div id="prjmodularcma" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-modular-cma-01.jpg" alt="CMA modular project slide" title="CMA">
								</li>
								<li class="projectslide">
									<img src="/images/portables/slide-modular-cma-02.jpg" alt="CMA modular project slide" title="CMA">
								</li>
								<li class="projectslide">
									<img src="/images/portables/slide-modular-cma-03.jpg" alt="CMA modular project slide" title="CMA">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjmodular1" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-modular-01.jpg" alt="Modular systems slide" title="The Knot">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjmodular2" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-modular-02.jpg" alt="Modular systems slide" title="Mono Serra">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjmodular3" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-modular-03.jpg" alt="Modular systems slide" title="Mantra Energy">
								</li>
							</ul>
						</div>
					</li>
					<li class="slide">
						<div id="prjmodular4" class="flexslider">
							<ul class="project">
								<li class="projectslide">
									<img src="/images/portables/slide-modular-04.jpg" alt="Modular systems slide" title="Nail Basics">
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div id="portfolionav" class="flexslider">
				<ul class="slides">
					<li>
						<img src="/images/portables/slide-modular-cma-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-modular-01-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-modular-02-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-modular-03-t.jpg" alt="" title="">
					</li>
					<li>
						<img src="/images/portables/slide-modular-04-t.jpg" alt="" title="">
					</li>
				</ul>
			</div>
		</div>
	</div>