<!DOCTYPE html>
<?php set_include_path($_SERVER['DOCUMENT_ROOT']); ?>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Trade show display, trade show design, tradeshow exhibits | Xibita | Exhibits</title>
	<meta name="description" content="Develop your perfect trade show display.  Xibita can help you with your trade show design and environments to improve your tradeshow results.">
	<meta name="keywords" content="trade show design, display booth, tradeshow exhibits, trade show display, display booth, trade show designs, display booths, tradeshow exhibit">

	<!-- styles & scripts -->
	<?php include("includes/common.php"); ?>
	<?php $section = "exhibits"; ?>
</head>
<body id="<?php echo $section ?>">
	<div id="top"></div>

	<!-- Header and Nav -->
	<?php include("includes/header.php"); ?>
 
	<!-- Slider -->
	<?php include("includes/slides-exhibits.php"); ?>
 
	<!-- Page heading -->
	<div class="row">
		<div class="eight columns centered">
			<h1>Trade show displays and environments that produce results.</h1>
		</div>
	</div>
  
	<!-- Three-up Content Blocks -->
	<div class="row">
		<div class="two columns offset-by-one">
			<ul class="vertical tabs subnav">
				<li class="active"><a href="/exhibits">Custom Exhibits + Environments</a></li>
				<li><a href="/retail-environments">Retail +<br>Corporate Environments</a></li>
				<li><a href="/custom-graphics">Custom Graphics</a></li>
			</ul>
			<?php include("includes/subnav-".$section.".php"); ?>
		</div>
		<div id="maincopy" class="four small-6 columns">
			<p class="subheading">Improve the results of your trade show or work environment. Xibita provides solutions.</p>
			<p>Research shows that people buy with emotion, and justify with logic. At Xibita, we listen, we develop, we design and we deliver extraordinary trade show displays and environments that get you noticed.</p>
			<p>If you are looking for insight, creativity and the expertise to connect your requirements with the power of innovative display technology, Xibita will meet your needs.</p>
		</div>
		<div class="two small-quote columns">
			<div class="panel callout">
				<?php include("includes/quotes.php"); ?>
			</div>
		</div>
		<div class="one columns"></div>
	</div>
  
	<!-- Awards -->
	<?php include("includes/awards.php"); ?>
  
	<!-- Footer -->
	<?php include("includes/footer.php"); ?>
  
	<!-- Included JS Files (Compressed) -->
	<?php include("includes/scripts-ftr.php"); ?>
  
	<!-- Initialize JS Plugins -->
	<script src="javascripts/app.js"></script>
	<script type="text/javascript">
		$(window).load(function() {
			// The slider being synced must be initialized first
			$('#portfolionav').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 232,
				itemMargin: 21,
				asNavFor: '#portfolio'
			});

			$('#portfolio').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav:false,
				selector: ".slides > .slide",
				sync: "#portfolionav"
			});

			$('#prj01').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj02').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj03').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj04').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj05').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj06').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj07').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj08').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj09').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj10').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj11').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj12').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj13').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj14').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj15').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj16').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj17').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj18').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj19').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj20').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj21').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj22').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj23').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj24').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj25').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});

			$('#prj26').flexslider({
				animation: "fade",
				directionNav: false,
				controlNav: true,
				animationLoop: true,
				selector: ".project > .projectslide",
				startAt: 0,
				slideshow: true,
				slideshowSpeed: 5000
			});
		});
	</script>
  
</body>
</html>
